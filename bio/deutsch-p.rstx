Paula Deutsch
=============

.. container:: dates

  -REPLACE-WITH-DATES-

Paula Deutsch wurde am 4. Juli 1900 als Paula Frankenthal in
Altenlotheim (Hessen-Nassau, Kreis Frankenau) als jüngste von acht
Geschwistern einer orthodoxen Familie, die seit einigen Generationen in
Altenlotheim lebte, geboren.  Paula hat die Höhere Mädchenschule in
Kassel besucht. 

Sie heiratete am 15.8.1923 in Frankfurt am Main `Salomon Deutsch`_.
Seit 1924 wohnte das Paar in Heidelberg, zunächst in der Landhausstraße
22, ab dem 1.10.1932 in der Werderstraße 17.  Das Paar hatte vier
Kinder.

Nach der Pogromnacht_ vom 9.11.1938 konnten die älteren Kinder der Familie 
Deutsch – Ernst (geb. 1924), Hanna-Charlotte (geb. 1926) und Frieda
Schlomith (geb. 1929) - am 20.4.1939 nach Schweden ausreisen. Im August
1940 wurden Salomon und Paula Deutsch mit ihrem jüngsten Kind Heinz
(geb. 1933) nach Ungarn ausgewiesen.

Am 19. März 1944 okkupierte das deutsche Reich Ungarn, um einen
bevorstehenden Separatfrieden zwischen der ungarischen Regierung und den
alliierten Mächten zu verhindern. Das hatte verhängnisvolle Folgen für
die JüdInnen in Ungarn: Anfang April 1944 wurden Salomon und Paula Deutsch
mit ihrem Sohn Heinz verhaftet und ins Internierungslager Budapest
gebracht. Heinz Deutsch wurde als Minderjähriger nach einiger Zeit
entlassen; er erhielt einen Schutzpass der schwedischen Botschaft in
Budapest und konnte durch das segensreiche Wirken von Raoul Wallenberg in
der schwedischen Botschaft den Krieg überleben. 

Paula Deutsch wurde, wie ihr Mann und 500.000 andere JüdInnen aus
Ungarn, nach Auschwitz verschleppt und dort ermordet. 

Ihr Sohn Heinz Deutsch kam 1947 nach Israel und nahm dort den Namen Yitzhak
Dishon an.  1960 übersiedelte er in die USA. Sein Studium an der Santa
Clara University in Kalifornien schloss er mit einem Master of Science
in Computerwissenschaften ab, danach arbeitete er 26 Jahre bei IBM. Er
heiratete, hatte zwei Kinder und drei Enkel. Unter dem Titel „Child of the
Holocaust“ erschien ein Buch über sein Verfolgungsschicksal.  Yitzhak
Dishon starb 1989 in Boston (USA).  Er ist in Israel begraben.

`Mehr zur Familie Deutsch <http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2012/2012_Salomon_Paula_Deutsch.pdf>`_

.. _Salomon Deutsch: deutsch-s.html
.. _Pogromnacht: http://de.wikipedia.org/wiki/Novemberpogrom%201938
