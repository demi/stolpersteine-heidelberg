Heinrich  Hochherr
==================

.. container:: dates

  -REPLACE-WITH-DATES-

.. figure:: hochherr-h.jpeg

Heinrich (Heinz) Hochherr ist Sohn des Heidelberger
Zigarrenfabrikanten `Simon Hochherr`_ und dessen erster Ehefrau Karoline. 

Er wurde am 3. Juli 1910 in Düsseldorf geboren.  Seine Mutter starb
bereits 1914, weshalb er während des ersten Weltkriegs in der Familie 
seines Onkels `Ferdinand Hochherr`_ lebte.  Nach der Heirat seines Vaters
mit `Ella Hochherr`_ 1919 und dessen Übersiedlung nach Heidelberg
zog er hier in der Brückenstraße 51 ein.  1920 wurde seine Halbschwester
`Liselotte`_ geboren.

Nach seiner Ausbildung zum Kaufmann arbeitete er in der Fabrik seines
Vaters und seines Onkels Ferdinand. 

Er konnte noch vor den Pogromen von 1938 in die Niederlande fliehen,
wo er im Oktober 1938 seine Verlobte `Margot Bähr`_ heiratete. 

Mit der Invasion der Wehrmacht 1940 holte ihn auch die
deutsche Unterdrückungsmaschinerie wieder ein.
1942 sollte er zu einem angeblichen Arbeitseinsatz in Deutschland
verpflichtet werden, tatsächlich haben ihn die Besatzungsbehörden 
zunächst im Durchgangslager Westerbork_ interniert und von dort
am 17. August 1942 nach Auschwitz deportiert.  Dort wurde er
am 18. August 1942 ermordet.

`Mehr zur Familie Hochherr <http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2012/2012_Fam_Simon_Hochherr.pdf>`_

.. _Simon Hochherr: hochherr-s.html
.. _Ella Hochherr: hochherr-e.html
.. _Ferdinand Hochherr: hochherr-fe.html
.. _Liselotte: hochherr-l.html
.. _Margot Bähr: baehr-m.html
.. _Westerbork: http://de.wikipedia.org/wiki/Durchgangslager%20Westerbork
