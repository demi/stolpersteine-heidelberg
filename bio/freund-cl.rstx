Clara Freund, geb. Dornberger
=============================

.. container:: dates

  -REPLACE-WITH-DATES-

.. figure:: freund-ad-cl.jpeg

   Clara Freund und ihr Mann Adolf David, ca. 1917.

Clara Dornberger wurde am 11. November 1884 in Straßburg geboren.

Am 26. Januar 1917 heiratete sie `Adolf David Freund`_, der in
Friedenszeiten mit seinem Vater in der Rohrbacher Straße 77a ein
Lebensmittelgeschäft mit Großhandlung betrieb und im ersten Weltkrieg
für die österreichisch-ungarische Armee kämpfte.

Am 13. Januar 1920 brachte sie die Tochter `Amalie`_, am 24. September
1925 den Sohn `Heinrich`_ zur Welt.

.. _Heinrich: freund-he.html
.. _Amalie: freund-am.html
.. _Adolf David Freund: freund-ad.html

Nach der Machtübergabe an die NSDAP mussten die Freunds infolge
wachsenden antisemitischen Drucks zunächst die Großhandlung, 1937 dann
auch das Ladengeschäft – das vor allem von Clara geführt wurde –
aufgeben.

Das Paar hatte bis 1938 mit Claras Mutter Jeanette Dornberger, ihrem
Schwiegervater Michael und dessen zweiter Ehefrau Gisela
zusammengewohnt.  Am 16. April 1938 starb ihr Schwiegervater, am 26.
September 1938 ihre Mutter; Gisela Freund starb am 22. Dezember 1939.

Im August 1938 konnte Claras Tochter Amalie über Frankreich in die USA
fliehen.  Spätestens, nachdem die Behörden ihren Mann nach der
Pogromnacht vom 9. November 1938 in das KZ Dachau verschleppt hatten,
war klar, dass auch die übrige Familie fliehen musste.  Clara galt für
die US-Behörden als Französin, und so konnte sie mit dem 14-jährigen
Heinrich im Januar 1940 auf der Einwanderungsquote für FranzösInnen dem
NS-Terror entfliehen.

Ihr Mann, wegen seiner galizischen Herkunft für die US-Behörden Pole,
musste zurückbleiben und fiel noch im Jahr 1940 den mörderischen
Verhältnissen im Lager Gurs_ zum Opfer.

Clara Freund siedelte sich in New York an und lebte dort bis zu ihrem
Tod am 18. Januar 1980.  Ihr Enkel Martin Blatt schrieb über sie:

.. pull-quote ::

  Clara Freund, my Oma, was a wonderful woman, strong, deeply caring, generous and never embittered.

`Mehr zu Familie Freund <http://stolpersteine-heidelberg.de/familie-freund.html>`_

.. _Gurs: http://de.wikipedia.org/wiki/Camp%20de%20Gurs
