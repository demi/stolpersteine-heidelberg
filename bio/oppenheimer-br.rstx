Bruno Oppenheimer
=================

.. container:: dates

  -REPLACE-WITH-DATES-

Bruno Oppenheimer wurde am 27. Juni 1904 in Heidelberg geboren als
viertes von sechs Kindern von Moritz Oppenheimer (1865 - 1946) und Marie
Oppenheimer, geb. Münzesheimer (1872 - 1969).  Brunos Vater war bis 1930
Heidelberger Stadtverordneter und bis 1931 Kaufmännischer Leiter der
Herrenmühle A.G.; die angesehene jüdische Familie lebte von 1917 bis
1931 in der Villa in der Sophienstraße 1. Dieses Gebäude wird heute vom
Kurfürst-Friedrich-Gymnasium als Schulhaus genutzt. 

Bruno war seit seiner Geburt gehörlos und litt seit seinem 15.
Lebensjahr an epileptischen Anfällen. Zunächst lebte er in seiner
Familie und wurde von einem Heidelberger Arzt als „immer ruhig,
fügsam, anhänglich“ beschrieben. 

Von 1910 bis 1914 besuchte er die Schule für Hörbehinderte (damals
„Taubstummenschule“ genannt) in Frankfurt, anschließend wurde er in
einer entsprechenden Heidelberger Einrichtung betreut. Seit dem
16.3.1922 war Bruno in der „Heil- und Pflegeanstalt für Epileptische“ in
Kork (bei Kehl) untergebracht.

1932 zog die Familie Oppenheimer in die Quinckestraße in Neuenheim. Da Bruno 
zu diesem Zeitpunkt bereits in Kork untergebracht war, kann das Haus in der 
Sophienstraße als sein letzter Wohnort in Heidelberg betrachtet werden.

Laut Akten wurden die Bewohner_innen der durch die Front bedrohten
Heilanstalt Kork bald nach Kriegsbeginn, nämlich am 3.9.1939, nach
Stetten im Remstal verlegt, kehrten aber am 24.7.1940 wieder nach Kork
zurück.

Über den gesamten Aufenhalt Bruno Oppenheimers hinweg sind Schreiben des
Vaters an die Leitung der Korker Einrichtung erhalten. Er erkundigt sich
nach dem Wohlergehen seines Sohnes, wehrt sich 1937 gegen die staatlich
verfügte Zwangssterilisation oder versucht vor seiner Flucht in die USA
durch finanzielle Angebote, die Lage Brunos, den sie „recht sehr lieb“
hätten, abzusichern.

Da viele Länder Einwanderungsbeschränkungen für psychiatrische Patienten
– und als ein solcher galt Bruno Oppenheimer nach den damaligen
Verordnungen – verfügt hatten, verzögerten sich vielleicht die
Fluchtpläne, die die Familie Oppenheimer wie viele andere jüdische
Familien nach der Machtübergabe an die Nationalsozialisten verfolgten. 

Ende Oktober 1940 war es dann zu spät: Moritz und Marie Oppenheimer 
wurden zusammen mit etwa 6.500 Juden aus Baden und dem damaligen Gau
Saarpfalz im Rahmen der sogenannten `Wagner-Bürckel-Aktion`_ in das Lager
Gurs_ in Südfrankreich verschleppt. Unter Mithilfe des Heidelberger
Stadtpfarrers der Heiliggeist-Kirche, `Hermann Maas`_, konnte das Ehepaar
im April 1941 schließlich in die USA entkommen. Alle Kinder bis auf
Bruno hatten Deutschland vermutlich bereits vor 1940 verlassen.

.. _Gurs: http://de.wikipedia.org/wiki/Camp%20de%20Gurs
.. _Wagner-Bürckel-Aktion: http://de.wikipedia.org/wiki/Wagner-B%C3%BCrckel-Aktion
.. _Hermann Maas: http://de.wikipedia.org/wiki/Hermann%20Maas%20%28Theologe%29

Was die Eltern nicht wissen konnten: Nur einen Tag nach ihrer eigenen
Deportation wurde ihr Sohn Bruno in die Tötungsanstalt Grafeneck_ auf der
Schwäbischen Alb gebracht.  Als der 36-jährige Bruno am 23. Oktober 1940
voller Freude und „in der Hoffnung, einen Ausflug mitmachen zu dürfen“
(so ein Geistlicher nach der Befreiung in einem Brief an Moritz
Oppenheimer) mit 42 anderen Patient_innen in einen der bekannten „grauen
Omnibusse“ mit Ziel Grafeneck stieg, ahnte er sicher nichts von seinem
Schicksal. 

.. _Grafeneck: http://de.wikipedia.org/wiki/T%C3%B6tungsanstalt%20Schloss%20Grafeneck

Da die Menschen, die im Rahmen der sogenannten „`Aktion T4`_“ nach
Grafeneck kamen, dort nach einer kurzen ärztlichen Untersuchung (um auf
den Formularen die Ermordungen durch vermeintlich plausible
Todesursachen kaschieren zu können oder interessante Sonderfälle  für
Forschungszwecke  herauszufiltern) meist am Tag ihrer Ankunft in einem
Schuppen vergast wurden, muss man vom 23.10.1940 als Todesdatum von
Bruno Oppenheimer ausgehen.

`Quelle <http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2012/2012_Bruno_Oppenheimer.pdf>`_

.. _Aktion T4: https://de.wikipedia.org/wiki/Aktion_T4
