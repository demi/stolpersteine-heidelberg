Dr. Dora Busch
==============

.. container:: dates

  -REPLACE-WITH-DATES-

.. figure:: busch-do.jpeg

   Dora Busch (Mitte) mit ihrem Töchtern 1932 am Werderplatz

Dora Jellinek wurde am 5. Januar 1888 in Wien als Kind von Camilla_ und
`Georg Jellinek`_ geboren.  Ihr Vater war seit 1891 Ordinarius für Staats-
und Völkerrecht, seit 1891 in Heidelberg, 1907 dann erster jüdischer
Rektor der Universität.  Ihre  Mutter wurde von Marianne Weber für die
Frauenbewegung gewonnen und beriet vor allem Arbeiterinnen juristisch.

1910 verließ ihr Vater die jüdische Gemeinde und ließ sich und seine
Familie evangelisch taufen. 

Nach ihrer Schulzeit studierte Dora Busch in Heidelberg Germanistik,
Romanistik und Anglistik mit dem Ziel, Gymnasiallehrerin zu werden. Hier
fiel sie 1910 mit einem Vortrag über Stefan George im lyrischen Seminar
auf.  Sie zitierte ausführlich Gedichte des „Meisters“ und stellte fest,
dass die Kommunikation zwischen Leser und Gedicht wegen der
Undeutlichkeit und Unverständlichkeit nicht funktioniert. Andererseits
lobte sie die „edle Sprache und Schönheit der Bilder“. Sie würdigte sein
„historisches Gefühl“ und beklagte, dass ihm „die Unmittelbarkeit des
Ausdrucks“ fehle. Deshalb könne er „niemals ein großer Lyriker sein“. 

Im Januar 1911 starb ihr Vater.  

Am 21. März 1911 heiratete Dora Busch den österreichischen Psychiater
Dr. Friedrich Busch.  1912 brachte sie ihre Tochter Erika, 1913 ihre
Tochter Gerda zur Welt. Das bedeutete natürlich, dass Dora das Studium
unterbrechen musste.

Das junge Paar war gut in die Heidelberger Gesellschaft integriert. Zu
ihrem Bekanntenkreis gehörten neben `Gustav Radbruch`_, der auch der Pate
ihrer Tochter Gerda war, der Philosoph `Emil Lask`_ und das Ehepaar 
Jaspers.

Schon im Jahr 1915 verlor Dora Busch ihren Mann, der als Bataillonsarzt
am ersten Weltkrieg teilgenommen hatte. Sie erzog dann alleine ihre
beiden Töchter und konnte ihr Studium 1922 mit der germanistischen
Dissertation über Lohengrin abschließen.

1923 wurde sie Lehramtsassessorin am Mädchengymnasium in der Plöck 40.
1933 wurde sie als Jüdin infolge des badischen Judenerlasses vom 7.
April 1933 im Juli wieder aus dem Staatsdienst entlassen. 

Ihr Gesuch, sie im Beruf zu belassen, wurde intensiv behandelt. Anfangs
hatte der damalige Kultusminister Otto Wacker viel Verständnis für die
Witwe eines gefallenen Frontkämpfers und erwirkte die Genehmigung, eine
Ausnahme von § 3 des Gesetzes zur `Wiederherstellung des
Berufsbeamtentums`_ zu machen.  Dabei stand er sicherlich unter dem
Eindruck des Schreibens von Busch, in dem sie ausführte, „sie könne nicht
recht glauben, dass unsere Regierung sich mit dem Vorwurf beflecken
will, dass sie den Kriegsopfern nun nach vielen Jahren noch einen
solchen Dank des Vaterlandes zuteil werden lässt.“ 

.. _Wiederherstellung des Berufsbeamtentums: http://de.wikipedia.org/wiki/Gesetz%20zur%20Wiederherstellung%20des%20Berufsbeamtentums

Nicht bekannt ist, warum der Minister seine Meinung änderte und
Dora Busch den Dienst definitiv verlassen musste. Er begründete das mit
dem Argument, dass er keinen Präzedenzfall schaffen wollte. Es ist
allerdings anzunehmen, dass Wacker nicht in den Ruf kommen wollte, als
judenfreundlich zu gelten. Dora Busch bekam immerhin eine geringe
finanzielle monatliche Bezahlung vom Ministerium, die bei 25 – 35 %
ihrer letzten Bezüge lag.  Als Auslandsjüdin und Kriegerwitwe wurde ihr
dieses Geld auch noch bis zum 1. August 1944, also noch sieben Monate
nach ihrer Deportation nach Theresienstadt, bezahlt. 

Um ihre Töchter und sich zu ernähren, gab Dora Busch zusätzlich
Nachhilfeunterricht und übernahm Schreibarbeiten. In dieser Zeit hatte
sie intensiven Kontakt mit nichtarischen Heidelberger Familien wie
Kaufman-Bühler und von Künßberg. Dieter Kaufmann-Bühler erhielt in ihrer
Wohnung in der Lutherstraße 42 Englisch- und Französisch Unterricht.
Ihre Freundin Dietlinde Raisig erinnert sich daran, wie sich ihre Kinder
in Neuenheim begegneten und nur zuwinken durften, da ihnen das Sprechen
miteinander verboten war.

Nachdem ihr Bruder Otto 1943 bereits an den Folgen von Misshandlungen
durch die Gestapo gestorben war, deportierten die Behörden Dora Busch am
10. Januar 1944 über Karlsruhe nach Theresienstadt_. Das Lager wurde erst
am 8. Mai 1945 von der Roten Armee befreit. Am 21. Juni 1945 kehrte sie
nach Heidelberg zurück. Während der Zeit im KZ durfte sie alle zwei
Monate eine Dreißig-Worte-Mitteilung an ihre Tochter Gerda schreiben. In
diesen kurzen Mitteilungen wird deutlich, wie die Lebensumstände und die
Versorgungslage in diesem „privilegierten“ Lager waren. Gerda wiederum
konnte alle vier Wochen einen Brief an ihre Mutter senden und
gelegentlich ein Lebensmittelpäckchen. 

.. _Theresienstadt: http://de.wikipedia.org/wiki/KZ%20Theresienstadt

In Heidelberg wurde Dora Busch 1946 als Studienrätin in den Schuldienst
eingestellt. Bereits 1948 führten die Strapazen von Theresienstadt dazu,
dass sie pensioniert wurde. Bis zu ihrem Tod lebte sie in der
Lutherstraße und beschäftigte sich intensiv mit der Psychoanalyse. Sie
beschrieb in einer „Skizze seines Lebenslaufs“ mit viel Liebe das kurze
Leben ihres Mannes fünfzig Jahre nach seinem Tod. Zudem veröffentlichte
sie eine psychoanalytische Deutung der Grimmschen Märchen. Diese Arbeit
widmete sie Annemarie Sänger, die ein psychagogisches Institut leitete
und bei der sie Kurse belegte. 

Auf ihre Töchter musste Dora schon sehr früh verzichten. 1952 starb ihre
Tochter Erika, die 1938 nach England emigriert war und dort geheiratet
hatte.  1959 starb auch ihr jüngeres Kind Gerda. Sie selbst lebte bis
April 1992, sie wurde also 104 Jahre alt.  Beerdigt ist sie auf dem
Friedhof Handschuhsheim.

`Quelle <http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2017/39_Dora_Busch.pdf>`_

.. _Gustav Radbruch: https://de.wikipedia.org/wiki/Gustav_Radbruch
.. _Georg Jellinek: https://de.wikipedia.org/wiki/Georg_Jellinek
.. _Camilla: jellinek-ca.html
.. _Emil Lask: https://de.wikipedia.org/wiki/Emil_Lask
