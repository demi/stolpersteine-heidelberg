# Schneller Hack, um auf der Basis eines Dateinamens die
# Koordinaten des Stolpersteins in-place ins HTML zu fummeln.
# Das erwartet die steine.tsv im übergeordneten Verzeichnis
# (so ist das im Makefile organisiert)
#
# Und gleich noch ein Hack obendrauf: wir fügen gleich noch
# die Geburts- und Sterbedaten in einen dates-container ein
# (vgl. README im Parent-Dir).
#
# Eine rst2html-Erweiterung wäre vielleicht insgesamt eleganter.


import io
import sys
import xml.etree.ElementTree as ET


STEIN_META = "../steine.tsv"


def parse_html(in_name):
	"""returns an ElementTree for the html file.

	This is a bit tricky because docutils inserts HTML entities
	that elementtree doesn't know.  Since doing it properly is hard
	in python3.7's etree, I hack around the problem by text replacement.

	(the proper fix is probably for docutils to emit well-formed XHTML).
	"""
	with open(in_name, "rb") as f:
		content = f.read()
	
	content = content.replace(b"&mdash;", "—".encode("utf-8"))

	return ET.parse(io.BytesIO(content))


def parse_coordinates(inF):
	res = {}
	for ln in inF:
		try:
			lat, long, _, addr, bio, rest = ln.split("\t", 5)
			if "\t" in rest:
				dates = rest.split("\t")[0]
			else:
				dates = rest

			bio = bio.split("/")[-1].strip()
			res[bio] = (lat, long, dates, addr.strip())
		except ValueError:
			# Zeile zu kurz, um eine biographie zu haben: hier ignorierbar
			pass
	return res


def main(to_process):
	ET.register_namespace("", "http://www.w3.org/1999/xhtml")

	with open(STEIN_META, "r", encoding="utf-8") as f:
		coos = parse_coordinates(f)

	if to_process not in coos:
		sys.stderr.write("*** Kein Eintrag in steine.tsv: {}?\n"
			.format(to_process))
		return
	lat, long, dates, addr = coos[to_process]

	tree = parse_html(to_process)

	links = tree.findall(".//*[@id='backlink']")
	if not links:
		sys.stderr.write("*** Kein Karteinlink: {}?\n".format(to_process))
	else:
		for link_to_fudge in links:
			href = link_to_fudge.get("href")
			link_to_fudge.set("href", f"{href}#19/{lat}/{long}")
			link_to_fudge.text = addr

	datelines = tree.findall(".//*[@class='dates docutils container']")
	if not datelines:
		sys.stderr.write("*** Kein dates-container: {}?\n".format(to_process))
	else:
		for dl in datelines:
			dl.text = dates

	tree.write(to_process)
	

if __name__=="__main__":
	main(sys.argv[1])
