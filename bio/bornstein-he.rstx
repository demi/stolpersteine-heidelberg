Hans Eduard Bornstein
=====================

.. container:: dates

  -REPLACE-WITH-DATES-

.. figure:: bornstein-lm-he-ir-ml-we.jpeg
  :figclass: full

  Hans Bornstein (zweiter von links) mit Geschwistern und Mutter 1945
  (Foto: Fam. Rosenblatt).

Hans Eduard Bornstein wurde am 12. April 1934 als Sohn des
Fremdenführers `Hugo`_ und seiner Frau `Luise Margarete Bornstein`_ in
Heidelberg geboren.  Nach Naziregeln galt er als
„Halbjude“, was ihn zahlreichen Repressalien aussetzte, behördlichen wie
auch privaten, etwa durch die Meidung, die ihm von den nichtjüdischen
Angehörigen seiner Mutter entgegenschlug.  Im Gegensatz zu als
„Volljuden“ eingestuften Kindern – die die Behörden zunächst
segregierten und 1940 fast durchweg deportierten – konnte Hans aber bis
1943 die normale Volksschule besuchen.

Hans hatte einen drei Jahre älteren Bruder, Werner_.  1937 und 1938
bekam er zwei kleine Schwestern, `Margarete Luise`_ und `Inge Ruth`_.

Mit Inkrafttreten der `Nürnberger Gesetze`_ 
im Jahr 1935entließ die Stadt seinen
Vater, der die Familie daraufhin mit Jobs außerhalb von Heidelberg über
Wasser hielt.  1937 wohnten die Bornsteins in der Belfortstraße 3, 1939
zogen sie in die Hauptstraße 111.

Angesichts immer weiter zunehmender Anfeindungen beschloss Werners
Mutter 1943, mit den Kindern in ein Versteck in einem kleinen Gartenhaus
auf einem Grundstück im Wald oberhalb des oberen Gaisbergwegs zu
fliehen, wo Freunde der Familie sie heimlich mit Lebensmitteln
versorgten.

Nach der Befreiung wohnte die Familie für eine Weile in der
Handschuhsheimer Landstraße 8.  1948 bot sich die Gelegenheit, jüdische
Kinder in das damals noch britische Mandatsgebiet Palästina zu bringen.
Hans-Eduard und seine Geschwister wurden dabei zunächst in eine
Sammelstelle im bayerischen Wolfratshausen gebracht, dort auf die
Ausreise vorbereitet und schließlich ausgeflogen.

Bei der Ankunft im Mandatsgebiet wurden den Kindern neue Namen
zugeteilt – Hans hieß danach Izrak Bornstein – und sie danach auf
verschiedene Kibbuzim verteilt.  Die Eltern kamen 1949 per Schiff nach
und versammelten ihre Kinder schließlich wieder.  Die Familie versuchte
zunächst, im Jordantal von Subsistenz-Landwirtschaft zu leben.
Später zog sie nach `Naharija`_, einer Stadt am Mittelmeer
zwischen Tel Aviv und der libanesischen Grenze.

Izrak Bornstein baute nach seinem Militärdienst bei der israelischen
Armee mit seinem Bruder Werner (nach der Einwanderung Abraham Kenan)
eine Fabrik auf und hatte fünf Kinder.  Er kehrte später mit zwei seiner
Kinder nach Deutschland zurück und starb im August 1995 in Heidelberg.

`Mehr zu Familie Bornstein <http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2020/43_Bornstein_-_Kopie.pdf>`_

.. _Hugo: bornstein-hb.html
.. _Inge Ruth: bornstein-ir.html
.. _Margarete Luise: bornstein-ml.html
.. _Luise Margarete Bornstein: bornstein-lm.html
.. _Werner: bornstein-we.html
.. _Hans Eduard: bornstein-he.html
.. _Naharija: http://de.wikipedia.org/wiki/Naharija
.. _Nürnberger Gesetze: https://de.wikipedia.org/wiki/N%C3%BCrnberger_Gesetze
