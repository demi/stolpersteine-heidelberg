Dr. Magda Pagel-Koll
====================

.. container:: dates

  -REPLACE-WITH-DATES-

.. figure:: pagel-ma.jpeg

   Magda Pagel, um 1930 (`CC-BY <https://commons.wikimedia.org/wiki/File:Walter_Pagel_Pathologist_and_medical_historian._Wellcome_L0027120.jpg>`_)

Maria Magdalena Emilie Koll wurde am 26. Juni 1894 in Köln (nach anderen
Angaben in Saarbrücken) geboren.  Vielleicht ist sie zum Medizinstudium
nach Berlin gegangen, wo ein großes  Vorbild  für  junge
Medizinerinnen, Franziska Tiburtius, eine Poliklinik für lungenkranke
Arbeiterfrauen gegründet hatte.  Magda Koll hat ihr Studium mit der
Promotion  wohl  während  des  Ersten  Weltkriegs abgeschlossen. 

Als sie 1920 in Berlin  den  vier Jahre jüngeren `Walter Pagel`_
heiratete, absolvierte sie gerade eine Ausbildung zur Fachärztin für
Lungenkrankheiten – es sollte auch ein Spezialgebiet ihres Mannes werden.

Noch in Berlin brachte sie am 4. Januar 1930 den gemeinsamen Sohn
Bernhard_ zur Welt.  Im  Oktober 1930 zog  die  kleine  Familie nach
Heidelberg, zunächst in die Keplerstraße, dann in die Beethovenstraße
45.

Schon 1933 mussten sie erneut umziehen, nachdem die Universität
Heidelberg ihrem Mann nach der Machtübergabe an die NSDAP fristlos
gekündigt hatte.

Am 14. Juni 1933 reisten die Pagels nach Frankreich aus und lebten
zunächst in Paris; da sich dort keine dauerhafte Existenzperspektive
ergab, zogen sie weiter nach Cambridge, wo sie Fuß fassen konnten.  1939
naturalisierten sich die Pagels, 1942 konvertierte Magda vom
römisch-katholischen Glauben zum Judentum.

Sie blieb weiter wissenschaftlich tätig, einerseits mit eigenen
Arbeiten, so etwa einer Untersuchung zur „Surgery of Jamerius“ im
Bulletin of the History of Medicine, Band 28, S. 471 (1954; derzeit
leider noch bei Jstor weggesperrt, Metadaten `bei PubMed`_), einer
Untersuchung einer von ihrem Schwiegervater entdeckten medizinischen
Handschrift aus dem 12. Jahrhundert und einer später in England
entdeckten weiteren Kopie dieses Werks.

.. _bei PubMed: https://pubmed.ncbi.nlm.nih.gov/13209241/

Vor allem aber hat sie wohl ihrem Mann zugearbeitet.  In einer Ausgabe
seines  Opus  magnum  über Paracelsus gedenkt der 84-jährige Walter
Pagel  dieser  lebenslangen  Gemeinschaft:

.. pull-quote::

  Er [Pagel] kann nicht schließen, ohne an Magda Pagel-Koll zu erinnern.
  Sie hat 57 Jahre ihres Lebens der unermüdlichen Fürsorge und Erhaltung
  seines Lebens und seiner  literarischen  Arbeit  gewidmet  und sie in
  allem geteilt zusätzlich zu ihren eigenen  Forschungen  über
  mittelalterliche Kunst und Medizin. Das vorliegende Buch und seine
  Ergebnisse verdankt sich einzig dieser  gemeinschaftlichen  Arbeit.



`Mehr zu Familie Pagel <http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2021/2021-fam-pagel.pdf>`_

.. _Bernhard: pagel-be.html
.. _Magda Koll: pagel-ma.html
.. _Walter Pagel: pagel-wa.html
.. _Gurs: http://de.wikipedia.org/wiki/Camp%20de%20Gurs
.. _Wagner-Bürckel-Aktion: http://de.wikipedia.org/wiki/Wagner-B%C3%BCrckel-Aktion
.. _Noé: http://de.wikipedia.org/wiki/No%C3%A9%20%28Haute-Garonne%29
.. _Récébédou: http://de.wikipedia.org/wiki/Portet-sur-Garonne
.. _Wiederherstellung des Berufsbeamtentums: http://de.wikipedia.org/wiki/Gesetz%20zur%20Wiederherstellung%20des%20Berufsbeamtentums
.. _Hermann Maas: http://de.wikipedia.org/wiki/Hermann%20Maas%20%28Theologe%29
.. _Polenaktion: http://de.wikipedia.org/wiki/Polenaktion
.. _Nürnberger Gesetze: https://de.wikipedia.org/wiki/N%C3%BCrnberger_Gesetze
