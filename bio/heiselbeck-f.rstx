Fanny Heiselbeck
================

.. container:: dates

  -REPLACE-WITH-DATES-

Fanny Heiselbeck wurde im April 1912 in Rohrbach als Fanny Storch
geboren.  Sie war die Tochter von `Paula Storch`_ und
`Jakob Isaak Storch, gen.  Stern`_, die 1909 aus dem polnischen Krosno
(Crossen/Oder) nach Rohrbach gezogen waren.  Ihre älteren Schwestern
Klara_, Eva und Lea waren noch in Polen zur Welt gekommen.  Fannys Vater
betrieb in Rohrbach ein Schuhgeschäft und war 1930 in die
Heinrich-Fuchs-Straße 41 gezogen.

Fanny Storch heiratete Mitte der 1930er Jahre `Leo Leser Heiselbeck`_.
Dessen Mutter war wie Fannys Mutter in Polen geboren worden, weshalb ihn
die deutschen Behörden im Zuge ihrer „Polenaktion_“ am 28. Oktober 1938
nach Polen deportierten.   Die überforderten polnischen Behörden
brachten ihn und Fannys Vater zunächst 
in einem Lager bei Zbaszyn (Bentschen) gleich hinter der Grenze unter.

Fanny Heiselbeck hingegen blieb zunächst in Deutschland. Im Rahmen der
staatlich betriebenen Ausraubung und Entmietung jüdischer Bürger
musste sie 1939 in das „Judenhaus_“ in der Hauptstraße 60 ziehen.
Dorthin zwangen die Behörden auch ihre Mutter Paula Storch, ihre
Schwestern Eva Rothschild und Klara Ziegler soie Klaras Tochter Ester_.

Im Juli 1939 wies die Polizeibehörde Fanny nach Polen aus. Mit
ihrem Mann, der bis dahin weiter im Lager Zbaszyn gelebt hatte,
zog sie nach Krakau. 

Auf einer Postkarte vom 31. Dezember 1939 steht ihre Adresse: Krakau,
Krakowska Straße 18 im alten jüdischen Viertel Kazimierz. Im März 1941
mussten alle jüdischen BewohnerInnen Krakaus in das neu ausgewiesene
Ghetto Podgorze umziehen. Die deutschen Behörden ließen den Stadtteil
mit Mauer und Stacheldraht abriegeln und seine BewohnerInnen in der
Folge fast alle ermorden. 

In der Wiedergutmachungsakte von Fannys Schwestern Lea Muniches und Eva
Rothschild in Kopie vorhandene Postkarten berichten davon, dass Leo und
Fanny noch von Krakau aus eigene Bemühungen für eine Ausreise über das
amerikanische Konsulat in Warschau anstrengen wollten, wobei sie Zweifel
äußern, ob das Konsulat noch „amtiert“. 

.. figure:: heiselbeck-karte.jpeg
  :figclass: full

  Postkarte von Fanny und Leo Heiselbeck vom 21. Dezember 1939 aus
  Krakau an Fannys Schwester Eva und ihren Schwager Sali Rothschild.

Eine letzte Nachricht findet sich auf einer Postkarte vom 4. März 1940.
Jakob und Paula, die in zwischen im Ghetto Gorlice lebenden Eltern,
teilen an die Adresse von Sali Rothschild in Mannheim mit: „Von Leo
und Fanny haben wir vorige Woche [also im Februar 1940] ein Schreiben
bekommen, daß sie sich soweit wohl befinden.“ Danach gibt es keine
Spuren der Eheleute Heiselbeck mehr. Sie gelten als verschollen.

`Mehr zu Fanny Heiselbecks Familie <http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2016/2016_Storch_Ziegler_Heiselbeck.pdf>`_

.. _Polenaktion: https://de.wikipedia.org/wiki/Polenaktion
.. _Paula Storch: storch-p.html
.. _Jakob Isaak Storch, gen. Stern: storch-ji.html
.. _Klara: ziegler-k.html
.. _Ester: ziegler-e.html
.. _Leo Leser Heiselbeck: heiselbeck-ll.html
.. _Fanny: heiselbeck-f.html
.. _Judenhaus: http://de.wikipedia.org/wiki/Judenhaus
