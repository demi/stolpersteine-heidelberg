Sophie Levy, geb. Bamberger
===========================

.. container:: dates

  -REPLACE-WITH-DATES-

.. figure:: levy-so.jpeg
  :alt: Portrait
  
  Sophie Levy um 1905.

Sophie Bamberger wurde am 30. April 1875 in Heilbronn geboren.  Am 8.
August 1898 heiratete sie den Heidelberger Textilunternehmer `Bernhard
Levy`_.  Gefeiert  wurde  in  festlichem  Rahmen  im Stuttgarter
Königsbau_, was auf einen gewissen Wohlstand des Brautpaares bzw. der
Eltern schließen lässt.

.. _Königsbau: http://de.wikipedia.org/wiki/K%C3%B6nigsbau

Sophie Levy brachte im August 1899 den gemeinsamen Sohn Albert zur Welt,
im Jahr 1907 den zweiten Sohn, Fritz_. Albert, der Erstgeborene, starb
am 22. August 1918 als Soldat im Ersten Weltkrieg, so dass Fritz Sophies
einziger überlebender Sohn war.

Das Ehepaar Levy wohnte seit 1932 in der Landfriedstraße 2.  1933 floh
ihr Sohn Fritz – der bis dahin im Staatsdienst gearbeitet hatte – vor
den beginnenden Repressalien der NSDAP-Regierung nach England.  1935
zogen die Eltern in die Zähringerstraße 4 um.

Sophie Levy starb am 13. September 1938, zwei Monate, bevor SA-Trupps
die Wohnung von ihr und ihrem Mann während der Reichspogromnacht
verwüsteten.

`Mehr zu Familie Levy <http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2021/2021-fam-levy.pdf>`_

.. _Sophie Bamberger: levy-so.html
.. _Fritz: levy-fr.html
.. _Bernhard Levy: levy-fr.html
.. _Gurs: http://de.wikipedia.org/wiki/Camp%20de%20Gurs
.. _Wagner-Bürckel-Aktion: http://de.wikipedia.org/wiki/Wagner-B%C3%BCrckel-Aktion
.. _Noé: http://de.wikipedia.org/wiki/No%C3%A9%20%28Haute-Garonne%29
.. _Récébédou: http://de.wikipedia.org/wiki/Portet-sur-Garonne
.. _Wiederherstellung des Berufsbeamtentums: http://de.wikipedia.org/wiki/Gesetz%20zur%20Wiederherstellung%20des%20Berufsbeamtentums
.. _Hermann Maas: http://de.wikipedia.org/wiki/Hermann%20Maas%20%28Theologe%29
.. _Polenaktion: http://de.wikipedia.org/wiki/Polenaktion
.. _Nürnberger Gesetze: https://de.wikipedia.org/wiki/N%C3%BCrnberger_Gesetze
