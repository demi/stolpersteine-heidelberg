Sara Snopek, geb. Isaak
=======================

.. container:: dates

  -REPLACE-WITH-DATES-

Sara Snopek wurde am 14. Juli 1867 in Nekla_, Provinz Posen (heute:
Woiwodschaft Großpolen), als Sara Isaak geboren. Am 14. Juli 1867
heiratete sie `Ludwig Snopek`_.  Sie brachte sieben Kinder zur Welt:
1895 Selma (Flucht 1935, gest. 1978 in New York), 1896 Klara (gest. 1920
in Heidelberg, wo sie als Verkäuferin gearbeitet hat), 1897 Martin
(gest. 1918 an der Westfront), 1899 Betty_ (ermordet vermutlich 1942 in
Auschwitz), 1900 Max (gest. 1986 in den USA), 1901 Joseph (gest. 1966 in
den USA) und 1906, bereits in Heidelberg, Fred (früher Saly, gest. 1993
in den USA).  Einem Sohn von Max, Karl-Heinz Dörr, gratulierte 2017 das
`Mitteilungsblatt der Gemeinde Edingen`_ zum 85. Geburtstag.

.. _Nekla: https://de.wikipedia.org/wiki/Nekla
.. _Mitteilungsblatt der Gemeinde Edingen: https://www.edingen-neckarhausen.de/fileadmin/Dateien/Dateien/AMB-Archiv/AMB_38_-_21.09.2017_-_digital.pdf

1905 zog die Familie – das Ehepaar, sechs Kinder und ein Dienstmädchen –
nach Heidelberg, wo sie nach anfänglich häufigem Wohnungswechsel 1907 in
der Blumenstraße 32 eine Obst- und Gemüsehandlung betrieb. Das
Adressbuch von 1914 vermerkt unter der Adresse: „Snopek S., Obst-,
Gemüse-, Eier- und Flaschenbierhandlung, Niederlage in Backwaren“.  In
diesem Jahr meldete Sara aber das Gewerbe ab, die Familie zog in die
Blumenstraße 24. 

Im ersten Weltkrieg starb ihr Sohn, ihr Mann kam verletzt zurück und
bezog als Schwerbeschädigter eine Kriegsrente.

Nach der Machtübergabe an die NSDAP konnten fast alle überlebenden
Kinder in die USA fliehen.  Zurück blieben Sara, ihr Mann und ihre
Tochter Betty, die 1935 mit ihrem Dienstmädchen Marianne „Marinka“
Pollorak in die 6-Zimmer-Wohnung mit Küche und Mädchenzimmer im zweiten
Stock der Rohrbacher Straße 51 zogen.

Spätestens nach der Reichspogromnacht war den verbliebenen Snopeks klar,
dass auch sie würden fliehen müssen.  Sara und ihr Mann konnten 23.
Februar 1940 in die USA ausreisen.  Die Papiere ihrer Tochter Betty
hingegen kamen zu spät; der NS-Apparat hat sie in Auschwitz ermorden
lassen.

Im Jahr 1943 feierte Sara Snopek in New York ihre goldene Hochzeit mit
Ludwig im Kreise ihrer Kinder.

Sie starb am 7.2.1945. Die in Heidelberg zurückgebliebene Marinka
schrieb dazu:

  Aber es ist ja kein Wunder, daß die arme Mama dies nicht überstehen
  konnte, nachdem sie alles verloren hatte. Was für ein schönes Heim
  hatten wir hier in Heidelberg und was für ein Unheil hat dieser Mörder
  über uns alle gebracht.

`Mehr zu Familie Snopek <http://stolpersteine-heidelberg.de/familie-snopek.html>`_

.. _Gurs: http://de.wikipedia.org/wiki/Camp%20de%20Gurs
.. _Wagner-Bürckel-Aktion: http://de.wikipedia.org/wiki/Wagner-B%C3%BCrckel-Aktion
.. _Ludwig Snopek: snopek-lu.html
.. _Betty: snopek-be.html
