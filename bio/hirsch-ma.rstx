Max Hirsch
==========

.. container:: dates

  -REPLACE-WITH-DATES-

Max Moses Hirsch wurde am 8. August 1867 als zweitjüngstes von neun
Kindern von Adelheid (geb. 1831 in Baiertal) und Alexander (geb. 1818 in
Walldorf) Hirsch in Wiesloch geboren.  Seine Familie väterlicherseits
lebte bereits seit drei Generationen in Wiesloch.

Im Jahr 1910 zog er von St. Johann bei Speyer in die Albert-Mays-Straße
11a und firmierte seitdem im Adressbuch als Kaufmann. Erst 1916 meldete
er beim Großherzoglichen Bezirksamt in Heidelberg sein Gewerbe als
Vertreter der „Rheinpfälzischen Eisenindustrie, Inhaber Kaiser und
Schlaudecker, St. Ingbert, Baumaschinen & Werkzeuge für
Bauuntersuchungen“ (später `Maschinenfabrik Otto Kaiser`_) an, das
er jedoch schon seit 1. April 1910 von Heidelberg aus betrieb. Aus einem
weiteren Brief an das Badische Bezirksamt Heidelberg von 1921, mit
schwungvoller Hand geschrieben, geht hervor, dass er nach dem Ersten
Weltkrieg diese „Vertretung der Rheinpfälzischen Eisenindustrie nicht
mehr habe und auf eigene Rechnung diese Artikel [nun] verkaufe und
einkaufe“. Der Briefkopf kündet selbstbewusst vom Stolz auf sein eigenes
kleines Unternehmen: 

.. figure:: hirsch-ma-brief.jpeg
  :figclass: full
  :alt: Faksimile eines Briefs von Max Hirsch mit Briefkopf

  (Stadtarchiv Heidelberg)

.. _Maschinenfabrik Otto Kaiser: http://de.wikipedia.org/wiki/Maschinenfabrik_Otto_Kaiser

Im Jahr 1933 zog seine jüngere Schwester `Flora Maienthal`_, deren Mann
bereits 1917 verstorben war, aus Mannheim zu ihm in die Mays-Straße.

Nach Angaben von Floras Tochter Gretel und einer früheren
Nachbarin haben Max Hirsch und seine Schwester zunächst noch in
gutbürgerlichen Verhältnissen gelebt. Im späteren
Entschädigungsverfahren wird berichtet von einer eingerichteten
5-Zimmer-Wohnung mit guten Möbeln, aber auch „wertvollen Gemälden von
bedeutenden deutschen Malern wie [Käthe] Kollwitz, dem Münchner Maler
[Fritz?] Burger, dem Maler [Karl?] Hofer sowie dem Karlsruher Maler
[Friedrich] Kallmorgen, aber auch Silber, Porzellan und Teppiche“.

Spätestens seit Anfang 1936 gab es sein Geschäft nicht mehr. Dies
erfahren wir aus der Antwort auf eine Anfrage der Heidelberger
Polizeidirektion vom 28. August 1937, ob der Betrieb noch bestünde. 
Immerhin ist im Jahr 1940 Max Hirsch im Adressbuch noch in der
Mays-Straße verzeichnet, wenn auch mit dem diskriminierenden
Zwangsbeinamen Israel.

Am 22. Oktober 1940 werden Max und seine Schwester Opfer der
`Wagner-Bürckel-Aktion`_: Die Behörden von Deutschland und
Vichy-Frankreich deportieren sie nach Gurs_.
Max Hirsch ist zu diesem Zeitpunkt 73 Jahre alt.

.. _Gurs: http://de.wikipedia.org/wiki/Camp%20de%20Gurs
.. _Wagner-Bürckel-Aktion: http://de.wikipedia.org/wiki/Wagner-B%C3%BCrckel-Aktion

Max Hirsch hatte eine Nichte in Lyon, Catherine Salomon.  Als diese
vom Transport der badischen Juden nach Gurs erfuhr, besorgte sie sich
die Erlaubnis, als Französin ins Camp de Gurs gehen zu dürfen.  Mit
ihrer Tochter, Renée Koster aus Sevres, sei sie „mit Koffern voll
Lebensmittel nach Gurs“ gefahren: „Dort haben wir unter sehr vielen
Tränen mit Flora Maienthal und deren Bruder Max Hirsch gesprochen, der
Stacheldraht zwischen uns.“ Sie berichtet weiter, dass bei ihrem zweiten
Besuch, wieder „mit Lebensmitteln und dem Lebensnotwendigsten“ bepackt,
ihre „Tante mit vielen anderen in der Baracke, isoliert, zum Sterben
bereit, mit Typhus“ daniederlag. Nichte und Tochter setzten sich nun
direkt mit der Präfektur in Pau in Verbindung, wo ihre „tragischen
Schilderungen der unmenschlichen Verhältnisse“ Gehör fanden und sie die
Erlaubnis erhielten „Flora Maienthal und deren alten Bruder, Max Hirsch,
auBerhalb des Lagers unterzubringen“ – Gurs lag in der nicht direkt aus
Berlin regierten Zone Frankreichs. 

So konnten Flora und ihr Bruder am 15. Juni 1941 das Lager Gurs
verlassen und in einem Zimmer in Tence_ im Département Haute-Loire
unterkommen.

.. _Tence: http://de.wikipedia.org/wiki/Tence

Während Max' Schwester im Juli 1943 in Tence starb, ist sein weiteres
Schicksal unbekannt.

Jedoch berichtete die bereits erwähnte ehemalige Nachbarin aus der
Albert-Mays-Straße, die das Geschwisterpaar noch aus der Zeit vor
der Deportation kannte, dass Max Hirsch nach dem Krieg noch einmal
zurückgekehrt sei.  Allerdings bezeichnete sie Max Hirsch als Maienthals
Sohn. Dieser sei früher häufig auf Reisen gewesen; das passt zu dem
Beruf Max Hirschs, aber nicht zu seinem Alter, war er doch vier Jahre
älter als seine Schwester.

.. _Flora Maienthal: maienthal-fl.html

`Mehr zu Flora Maienthal und ihrem Bruder <http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2016/2016_Max_Hirsch_Flora_Maienthal.pdf>`_
