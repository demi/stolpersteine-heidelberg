Bernhard Pagel
==============

.. container:: dates

  -REPLACE-WITH-DATES-

.. figure:: pagel-be.jpeg

  Bernhard Pagel mit seiner Frau Annabel, 1958.  Rechte bei der Royal
  Socieny.

Bernhard Ephraim Julius Pagel wurde am 4. Januar 1930 in Berlin als
Sohn der ÄrztInnen Magda_ und `Walter Pagel`_ geboren.  Im  Oktober
1930 zog  die kleine  Familie nach Heidelberg, zunächst in die
Keplerstraße, dann in die Beethovenstraße 45.  Da die Universität
Bernhards Vater nach der Machtübergabe an die NSDAP kündigte, zogen die
Pagels weiter nach Cambridge, UK, und dann weiter in den Norden von
London.

Während sein Vater ihn gerne als Arzt gesehen hätte und seine Mutter ihn
zu Molkularbiologie drängte, wollte Pagel zunächst klassische Philologie
studieren.  Als er sich aber am Sidney Sussex College in Cambridge
einschrieb, entschied er sich doch für Naturwissenschaften.  Eine in
einem `Nachruf bei der Royal Society`_ erzählte Anekdote aus seiner
späteren Universitätskarriere mag einen Eindruck vom komplizierten
Verhältnis Pagels zu seinen Eltern, vor allem zu seinem recht
autoritären Vater, werfen:

.. _Nachruf bei der Royal Society: https://royalsocietypublishing.org/doi/10.1098/rsbm.2020.0004

.. pull-quote::

  Bernhard hatte zwei research students [in etwa: Doktoranden] zu Gast;
  die drei hatten Squash gespielt.  Das Telefon klingelte, er verließ
  den Raum, um den Anruf anzunehmen.  Als er zurückkam, war sein Gesicht
  aschgrau.  Es stellte sich heraus, dass der Anruf von seinem Vater
  gekommen war. Dieser hatte gehört, dass Bernhard so leichtsinnig und
  unbedacht gewesen war, mit Studenten Sport zu treiben und hatte sich
  nun in harscher Kritik an Bernhard ergangen.

1950 erhielt er seinen Bachelor von der Uni Cambridge.  Zur Promotion
ging er an die dortige Sternwarte, 1952/53 ging er mit einem
Fulbright-Stipendium nach Michigan, 1955 wurde er mit einer Arbeit über
Sonnenphysik promoviert, die sich bereits Computersimulationen bediente.

Er heiratete 1958 Annabel Ruth Tuby, mit der er drei Kinder hatte: Celia
Ann (geb. 1959), David Benjamin (geb. 1961) und Jonathan Francis (geb.
1966).

Er arbeitete  von 1956 bis 1990  als Astronom  am Royal Greenwich
Observatory und zugleich als Professor in Sussex. Grundlegend sind seine
Arbeiten zur Zusammensetzung von Gasnebeln (z.B.
`doi:10.1093/mnras/189.1.95`_), zur Heliumproduktion im Urknall (was
viel zur Natur der dunklen Materie aussagt, z.B.
`doi:10.1093/mnras/255.2.325`_) oder auch zur Entstehung der Elemente in
der Umgebung der Sonne (z.B.  `doi:10.1093/mnras/172.1.13`_).

.. _doi:10.1093/mnras/172.1.13: https://doi.org/10.1093/mnras/172.1.13
.. _doi:10.1093/mnras/255.2.325: https://doi.org/10.1093/mnras/255.2.325
.. _doi:10.1093/mnras/189.1.95: https://doi.org/10.1093/mnras/189.1.95

Nachdem er 1990 in England pensioniert worden war, nahm er eine Stelle
am Nordic Institute for Theoretical Physics in Kopenhagen an, wo er bis
1998 weiterarbeitete.  Bernhard Pagel starb am 14. Juli 2007 in
Ringmer_, einem Dorf im östlichen Sussex.

.. _Ringmer: https://en.wikipedia.org/wiki/Ringmerhttps://en.wikipedia.org/wiki/Ringmer

`Bernhard Pagel in der Wikipedia <https://en.wikipedia.org/wiki/Bernard_Pagel>`_

`Mehr zu Familie Pagel <http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2021/2021-fam-pagel.pdf>`_

.. _Bernhard: pagel-be.html
.. _Magda: pagel-ma.html
.. _Walter Pagel: pagel-wa.html
.. _Gurs: http://de.wikipedia.org/wiki/Camp%20de%20Gurs
.. _Wagner-Bürckel-Aktion: http://de.wikipedia.org/wiki/Wagner-B%C3%BCrckel-Aktion
.. _Noé: http://de.wikipedia.org/wiki/No%C3%A9%20%28Haute-Garonne%29
.. _Récébédou: http://de.wikipedia.org/wiki/Portet-sur-Garonne
.. _Wiederherstellung des Berufsbeamtentums: http://de.wikipedia.org/wiki/Gesetz%20zur%20Wiederherstellung%20des%20Berufsbeamtentums
.. _Hermann Maas: http://de.wikipedia.org/wiki/Hermann%20Maas%20%28Theologe%29
.. _Polenaktion: http://de.wikipedia.org/wiki/Polenaktion
.. _Nürnberger Gesetze: https://de.wikipedia.org/wiki/N%C3%BCrnberger_Gesetze
