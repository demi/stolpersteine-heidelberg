Guido Leser
===========

.. container:: dates

  -REPLACE-WITH-DATES-

.. figure:: leser-g.jpeg

  Guido Leser, ca. 1919.  Quelle: Oskar Gehring: Die verfassungsgebende
  badische Nationalversammlung, Karlsruhe 1919

Im August des Jahres 1914 dankte das badische Kultusministerium den
beiden Söhnen des verstorbenen Nationalökonomen Emanuel Leser für die
„großherzige Schenkung" der väterlichen Bibliothek an die Universität
Heidelberg und die Handelshochschule Mannheim. Walter und Guido Leser
waren damals als Juristen am Amtsgericht Mannheim tätig. Hier erreichte
beide Brüder kaum 19 Jahre später – im Mai 1933 – das
Entlassungsschreiben der badischen Regierung.

Von den dreizehn Mannheimer Amtsrichtern, die
zwischen 1933 und 1935 entlassen wurden, weil sie staatlicherseits als
Juden definiert worden waren, haben vier Verfolgung und Exil
überlebt, fünf wurden in Lagern ermordet, vier wurden zur „Flucht in den
Tod“ getrieben, wie es auf einem Stolperstein für Guido Leser vor dem
ehemaligen Badischen Landtag heißt.

Guido Leser wurde am 16. Oktober 1883 in Heidelberg als drittes Kind des
Ehepaars Ida und `Emanuel Leser`_ geboren.  Sein Vater gehörte dem
Synagogenvorstand an, Schilderungen der toleranten und heiteren
Atmosphäre im Haus Kaiserstraße 2 finden sich in manchen Erinnerungen
der damaligen Zeit.

Nach einem Studium der Rechtswissenschaften promovierte Guido Leser 1908
bei Georg Jellinek über „Das Wahlprüfungsrecht des Deutschen
Reichstags". Die  Dissertation schließt mit dem für einen 25-jährigen
Juristen bemerkenswerten Satz: „Alles in allem: es sind schon
schwierigere Probleme der Organisation gelöst worden – wo ein Wille ist,
ist auch ein Weg.“ Der Satz bekundet gleichermaßen Lesers Vertrauen in
die Rechtsprechung wie in die Politik.

Im Jahr seiner Promotion heiratete Leser `Irmingard Meyer`_, die 1915
den gemeinsamen Sohn Conrad_ zur Welt brachte.

Der junge Gerichtsassessor begann sich schon bald politisch zu
engagieren - zunächst als Gründungsmitglied der Fortschrittlichen
Volkspartei, die er ab 1912 im Heidelberger Bürgerausschuss vertrat.
Sein damaliger Fraktionskollege `Gustav Radbruch`_ schrieb später in
Erinnerung an Guido Leser: „Wir lernten, daß die mühevolle soziale
Kleinarbeit eine wertvolle Vorschule zur höheren Politik ist.“

1917 erhielt Leser den Orden des Roten Kreuzes. Vermutlich hatte er sich
um die zahlreichen Heidelberger Lazarette gekümmert,
deren Organisation zum Teil in den Händen `Max Webers`_ lag.

Gemeinsam mit dessen Ehefrau `Marianne Weber`_ und Eberhard Gothein zog
Guido Leser 1919 für die neu gegründete Deutsche Demokratische Partei in
die verfassungsgebende badische Nationalversammlung ein und wurde
Mitglied der Kommission für Justiz und Verwaltung.  Sein Freund Hugo
Marx schrieb später: „Dr. Guido Leser war einer der letzten, die an der
fortschreitenden Entwicklung und Verwirklichung des Liberalismus mit
fast naiv zu nennender Gläubigkeit hingen.“

Nur einmal ergriff Guido Leser das Wort in eigener Sache: Während der
„Judendebatte" am 22. Juni 1920 – es ging um antisemitische Hetze an den
badischen Hochschulen – sprach er sein Credo als jüdischer Deutscher:
„Ich kenne kein anderes Vaterland als das deutsche.“

Bei den badischen Landtagswahlen 1921 verloren rund zwei Drittel der DDP-Abgeordneten ihre Mandate – unter ihnen auch Guido Leser. Er kehrte in seinen Richterberuf zurück, scheint aber weiter politisch gearbeitet zu haben.  

Schon 1929 stellte die NSDAP im Badischen Landtag einen Antrag „Gegen
die Verjudung der Justiz“, der damals keine Mehrheit fand. Nach der
März-Wahl 1933 notierte Goebbels in seinem Tagesbuch: „ Als nächstes
Land ist Baden an der Reihe.“ Nur wenige Wochen später erhielt Guido
Leser Berufsverbot.

1936 zogen Guido Leser und seine Frau nach Berlin, möglicherweise,
weil sie glaubten, in einer Großstadt sicherer zu sein.  In der Tat
entgingen sie so der `Wagner-Bürckel-Aktion`_ von 1940, während der die
lokalen Behörden fast alle JüdInnen aus Baden deportierten.  Doch
erreichten die Deportationen 1942 auch Berlin.
In Kenntnis des Grauens, das sie erwartete, tötete sich das Ehepaar
Leser mit Schlaftabletten, als die Behörden sie aufforderten, sich
zur Deportation nach Theresienstadt zu melden.

.. _Wagner-Bürckel-Aktion: http://de.wikipedia.org/wiki/Wagner-B%C3%BCrckel-Aktion

„Dr. Guido Leser und seine Ehefrau Irmingard, geb. Meyer, sind am 26.
Oktober 1942 in Berlin durch die Gestapo in den Tod getrieben worden“,
schrieb Walter Leser am 28. Juli 1948 über den Suizid seiner
Schwägerin und seines Bruders an das Amtsgericht Mannheim „In Sachen
Wiedergutmachung“. Walter Leser selbst starb im Oktober 1948 an den
Folgen seiner Deportation nach Gurs_.

Die Grabstätte des Ehepaars Leser befindet sich auf dem Jüdischen
Friedhof Berlin-Weißensee.

`Mehr zur Familie Leser
<http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2015/2015_Familie_Leser.pdf>`_

.. _Gustav Radbruch: https://de.wikipedia.org/wiki/Gustav_Radbruch
.. _Conrad: leser-c.html
.. _Irmingard Meyer: leser-i.html
.. _Emanuel Leser: https://de.wikipedia.org/wiki/Emanuel_Leser
.. _Max Webers: https://de.wikipedia.org/wiki/Max_Weber
.. _Marianne Weber: https://de.wikipedia.org/wiki/Marianne_Weber_(Frauenrechtlerin)
.. _Gurs: http://de.wikipedia.org/wiki/Camp%20de%20Gurs
