Ludwig Seligmann
================

.. container:: dates

  -REPLACE-WITH-DATES-

.. figure:: seligmann-fr-l.jpeg
  :alt: Friedrich und Ludwig Seligmann
  
  Ludwig Seligmann als Sechsjähriger im Märzgarten (Märzgasse Ecke
  Plöck) mit seinem Vater auf Fronturlaub

Ludwig Seligmann wurde am 24. April 1910 in die Heidelberger
Bäckerfamilie Flora_ und `Friedrich Seligmann`_ geboren.  Die Seligmanns
wohnten in der
Plöck 34 und führten dort auch ihr Geschäft.  Er wuchs in der Heidelberger
Altstadt auf, er ging hier zur Schule und spielte im Fußballverein, hier
machte er in der väterlichen Bäckerei seine Lehre und verbrachte seine
Gesellenzeit. Seinen Meister erwarb er 1935 in Stuttgart.

Ebenfalls im Jahr 1935 war Ludwig mit einer „arischen” Frau liiert, was
der Gestapo zur Anzeige gebracht wurde, die daraufhin plante, ihn zu
„verhören“.  Er wurde aber von einem SA-Mann, einem Fußballkameraden aus
der Zeit vor dem Ausschluss jüdischer SportlerInnen aus den
Sportvereinen, gewarnt: „Lutz, pack ein paar Sachen und verschwinde, die
planen, dich morgen ganz früh zu holen.” 

So gelang ihm bereits 1935 die Flucht nach Uruguay.  1938 konnte er
seine Eltern nachholen.

Ludwig Seligmann hat in Uruguay eine Familie gegründet. Er heiratete
Pauline Boksar, die 1919 in Uruguay geboren wurde. Die Tochter Flora
wurde 1950 dort geboren. 

Nachdem die elterlichen Pläne, mit den aus Deutschland mitgebrachten
Maschinen in Montevideo eine neue Existenz aufzubauen, gescheitert
waren, kehrte Ludwig mit seinem Vater – die Mutter war 1948 verstorben –
1951 nach Deutschland zurück, wenig später folgte die Ehefrau mit der
kleinen Tochter den beiden. 

Ein Sohn wurde 1952 in Mannheim geboren, er starb noch am Tag seiner
Geburt.  Eine weitere Tochter, Esther, kam 1953 in Heidelberg zur Welt.
Die Ehefrau starb bereits 1957 in Mannheim, wo die Familie eine kleine
Gaststätte betrieb. Ludwig Seligmann ist am 24. Mai 1998 in Heidelberg
gestorben.  Seine Grabstelle befindet sich auf dem jüdischen Friedhof
am Bergfriedhof.

`Mehr zur Familie Seligmann <http://stolpersteine-heidelberg.de/familie-seligmann.html>`_

.. _friedrich seligmann: seligmann-fr.html
.. _flora: seligmann-fl.html
