Mina Ledermann
==============

.. container:: dates

  -REPLACE-WITH-DATES-

Mina Ledermann wurde am 27. September 1879 in Weikersheim an der Tauber
als Mina Ascher geboren.  Sie heiratete 1903 den Hoffenheimer
Bäckersohn `Max Ledermann`_ und brachte nennenswert Kapital in die Ehe,
das vermutlich wesentlich die Gründung eines Geschäfts für
„Manufakturwaren“ (Textilien) in der Neuen Straße 15 in Hoffenheim
ermöglichte.

Das Paar war wirtschaftlich erfolgreich und im Leben sowohl der
jüdischen als auch der sekularen Gemeinde aktiv.  Nach 1933 wurde die
Situation für JüdInnen aber auch in Hoffenheim schwieriger. Spätestens
1937 gaben die Ledermanns ihr dortiges Ladengeschäft auf und zogen in
die Endemannstraße 11 in der Heidelberger Weststadt, wo sie noch einen
Versandhandel betrieben.

Auch diesen stürmten Heidelberger Bürger im Zuge der Pogromnacht_ vom 9.
November 1938.  Eine Nachbarin aus der Hildastraße berichtet von den
Plünderungen um etwa 15 Uhr Nachmittags:

.. _Pogromnacht: http://de.wikipedia.org/wiki/Novemberpogrom%201938

.. pull-quote::

  Die Mutter hat am Fenster gestanden, die Arme in die Hüften gestemmt,
  und hat gesagt: „Jetzt sieh dir das an, was die da machen!“ Ich war 13
  Jahre alt und die Mutter hat mich rübergeschickt, damit ich Frau
  Ledermann helfe, all die Sachen, Stoffe, Knöpfe usw. wieder
  einzuräumen. Aber ein Nazi sagte zu mir: Du solltest dich schämen, als
  deutsches Mädchen einem jüdischen Weib zu helfen.  Mach dass du fort
  kommst, sonst passiert was! 

Während Verwandte ihres Mannes noch in die USA entkommen konnten,
blieben Mina und Max Ledermann trotz immer weitergehender Entrechtung 
(z.B. war ihnen seit dem 1. Dezember 1939 jeder Handel untersagt) in
Heidelberg.  So wurden auch sie am 22. Oktober 1940 in `den großen
Deportationszug`_ der JüdInnen in Baden und der Pfalz in das
Konzentrationslager Gurs_ gezwungen.

.. _den großen Deportationszug: http://de.wikipedia.org/wiki/Wagner-B%C3%BCrckel-Aktion
.. _Gurs: http://de.wikipedia.org/wiki/Camp%20de%20Gurs

In ihrer Leidenszeit im Camp de Gurs erhielt das Ehepaar Ledermann
Pakete von ihrer Nichte Henriette Sandler aus Paris, die sich selbst in
schwierigen wirtschaftlichen Verhältnissen befand. Offenbar waren die
Ledermanns der Meinung, dass diese schlimme Zeit enden würde und sie
wieder in ihre vorherigen Verhältnisse zurückkehren könnten, denn sie
sicherten der Nichte zu, ihr diese Ausgaben zu erstatten. 

Die deutschen Behörden haben Mina Ledermann und ihren Mann am
10. August 1942 vom Durchgangslager Drancy aus nach Auschwitz
deportiert.  Dort haben sie sie aller Wahrscheinlichkeit nach gleich
nach der Ankunft ermorden lassen.

`Mehr zu Mina und Max Ledermann <http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2016/2016_Max_Mina_Ledermann.pdf>`_

.. _Max Ledermann: ledermann-ma.html
