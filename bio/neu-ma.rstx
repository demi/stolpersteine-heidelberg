Maximilian Neu
==============

.. container:: dates

  -REPLACE-WITH-DATES-

.. figure:: neu-ma.jpeg

Maximilian Neu wurde am 5. April 1877 als Sohn des Kaufmanns Lazarus Neu und
seiner Frau Wilhelmine, geb. Reinhardt, in Freinsheim, Bayerische Pfalz,
geboren. 

Lebenslauf und Ausbildung waren die des zielstrebigen deutschen
Akademikers. Nach dem Abitur schrieb er sich 1897 in Heidelberg für
Medizin ein, hörte aber auch über Richard Wagner, Schiller und Dürer. 

In der Königlich Bayerischen Armee diente er als
„Einjährig-Freiwilliger“ und wurde später Oberarzt der Reserve. Mit
seiner Promotionsarbeit über Blutdruckmessung gewann er 1902 den Preis
der Fakultät. Im selben Jahr wurde er als Arzt approbiert. 1904 begann
er als Volontärassistent unter Geheimrat `von Rosthorn`_ seine Laufbahn als
Gynäkologe und Geburtshelfer in Heidelberg und wurde 1908 erster
Oberarzt.

.. _von Rosthorn: https://de.wikipedia.org/wiki/Alfons_von_Rosthorn

In der Habilitationsschrift von 1908 „Untersuchungen über die Bedeutung
des Suprarenins für die Geburtshülfe“ zeigte Neu, dass die nach
langwieriger Entbindung gefürchteten, häufig tödlichen Nachblutungen aus
der Gebärmutter durch diese erst zehn Jahre zuvor entdeckte Substanz
gemildert werden konnten.

Einen wesentlichen Beitrag hat Maximilian Neu 1910 für die Entwicklung
der Allgemeinnarkose geleistet, nämlich die exakte Dosierung von Lachgas
und Sauerstoff über Messröhren, die gerade für die Flussmessung bei
Leuchtgas patentiert worden waren. Mit seiner Konstruktion, dem Urahn
moderner Narkosegeräte, ließ sich zusammen mit der Injektion von
Scopolamin und Morphin eine ausreichende Narkosetiefe für alle
gynäkologischen Operationen erreichen, während die Patientinnen
zuverlässig vor Sauerstoffmangel geschützt waren.

Am 25.9.1912 heiratetete er in Heidelberg `Louise Baruch`_.

.. figure:: neu-lo-ma.jpeg

  Maximilian Neu mit seiner Frau Lousie

.. _Louise Baruch: neu-lo.html

1914 wurde er außerordentlicher Professor, verließ 1919 die Universität,
hielt aber bis 1933 gut besuchte Lehrveranstaltungen. Er betrieb
zunächst eine Praxis in der Brückenstraße 51 und eröffnete 1919 in dem
Villenanwesen Zähringerstraße 15 eine „Privatklinik für Geburtshilfe und
Frauenkrankheiten“. 1929 hatte sie 16 Betten; drei Assistenten und drei
Rotkreuz-Schwestern, eine OP-Schwester und eine Hebamme waren
ange­stellt. Im Garten hielt man Störche als Ausweis des Betriebszwecks.
Das Haus genoss einen sehr guten Ruf, Neu war wegen des fürsorglichen
Umgangs und der Sorge um rasche Wiederherstellung der äußeren
Erscheinung der Wöchnerinnen sehr geschätzt.

Seit 1927 wohnte das Ehepaar Neu in der Zähringerstraße 19,
1934 zogen sie in das Klinikgebäude in der Zähringerstraße 15.

Mit Beginn der NS-Herrschaft wurde Maximilian Neu als jüdischer
Hochschullehrer aus der Universität gedrängt. Zwar hatte er sich 1897
als „jüdisch“ in Heidelberg eingeschrieben, war jedoch ab 1908
„freireligiös“ geführt, ließ sich 1918 taufen und konnte sich kaum als
jüdisch gefühlt haben. Aber am 20.4.1933 wurde Neu „nun als
außerordentlicher Professor der Universität bis auf Weiteres beurlaubt“.
Zwar konnte er noch eine Ausnahmeregelung des „Gesetzes zur
`Wiederherstellung des Berufsbeamtentums`_“ beanspruchen, verlor aber im
Oktober 1933 die Lehrbefugnis endgültig. Mit dem Entzug der
Kassenzulassung jüdischer Ärzte 1933 und der Ächtung „arischer“
Patienten, die an ihren jüdischen Ärzten festhielten, verschwand Neus
Klinik 1936 aus dem Branchenverzeichnis. Bis zum Berufsverbot für
„Nichtarische Ärzte“ 1938 fand sich unter dieser Rubrik seine
Privatpraxis.

.. _Wiederherstellung des Berufsbeamtentums: http://de.wikipedia.org/wiki/Gesetz%20zur%20Wiederherstellung%20des%20Berufsbeamtentums

Der Verhaftung nach der Pogromnacht_ 1938 – nach der die
Behörden fast alle anderen von ihnen als jüdisch definierten Männer in Heidelberg
nach Dachau_ deportieren – war Maximilan Neu dank lautstarker
Intervention von NachbarInnen entgangen. So
hoffte das Ehepaar Neu offenbar, von der existentiellen Bedrohung nicht
wirklich betroffen zu sein. Falls doch, sei er vorbereitet, soll Neu
gesagt haben. Jeder Willkür gewärtig, sahen viele den Suizid als „letzte
Bastion der Selbstbestimmung“. Schon im Testament vom August 1938
hatten Maximilian und Zilla Neu einen Teil ihres Vermögens Pfarrer Maas
für wohltätige Zwecke vermacht.

.. _Dachau: http://de.wikipedia.org/wiki/KZ%20Dachau
.. _Pogromnacht: http://de.wikipedia.org/wiki/Novemberpogrom%201938

Die Neus waren seit Anfang des Jahrhunderts mit der Familie des
Kaufmanns Heinrich Diffené befreundet gewesen, die in
der Kleinschmidtstraße wohnte. Seit Diffené selbst 1916 gefallen war, 
war Maximilian Neu Vormund
des Sohnes Karl-Heinz.  Als am 21. Oktober 1940 die erste
Massendeportation deutscher JüdInnen („`Wagner-Bürckel-Aktion`_“)
abzeichnete, bot Frau Diffené, nun auch enge Nachbarin, im Herbst 1940
ihre Wohnung zum Übernachten an, damit das Ehepaar bei Gefahr nicht
zuhause angetroffen würde. 

Dennoch verbrachten  Maximilian und Zilla Neu den Abend des 21. Oktober 1940 in
ihrer Wohnung mit `Pfarrer Maas`_. Der Polizeibeamte, der sie am nächsten
Morgen abholen sollte, kannte sie und entschuldigte sich für die
Maßnahme. Das Paar bat ihn, sich zum Zähneputzen zurückziehen zu dürfen.
Dann nahmen sie auf ihrem Bett Zyankali.

Denny „Israel“ und Zilla „Sara“
Neu wurden am 25.10.1940 im „Nichtarierfeld“ des Bergfriedhofs im Winkel
zwischen Steigerweg und Bahnlinie von Hermann Maas beigesetzt.  1949
wurden Maximilian und Zilla Neu zu denen gebracht, denen sie sich
zugehörig geglaubt hatten: Prälat Maas ließ die beiden auf den
allgemeinen Teil des Bergfriedhofs umbetten. Das von ihm erworbene
Nutzungsrecht für das Grab V 427 ist 1981 erloschen.

.. _Wagner-Bürckel-Aktion: http://de.wikipedia.org/wiki/Wagner-B%C3%BCrckel-Aktion
.. _Pfarrer Maas: http://de.wikipedia.org/wiki/Hermann%20Maas%20%28Theologe%29

`Mehr zum Ehepaar Neu <http://stolpersteine-heidelberg.de/m-l-neu.html>`_
