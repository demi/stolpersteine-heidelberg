Irmingard Leser
===============

.. container:: dates

  -REPLACE-WITH-DATES-

Irmgard (später: Irmingard) Axelena Johanna Roberta Leser, geb. Meyer,
kam am 1. Mai 1888 in Göttingen zur Welt, wo ihr Vater, der Chemiker
`Victor Meyer`_, seit 1885 an der Universität lehrte.  Sie war die jüngste
von fünf Schwestern.

Ihre Mutter Hedwig Meyer, geb. Davidson, ist 1851 in Posen geboren. Sie
entstammte einer väterlicherseits jüdischen, mütterlicherseits
protestantischen Familie. 1885 konvertierte auch ihr Mann, Victor Meyer,
zum Protestantismus, ihre fünf Kinder wurden protestantisch erzogen.

1889 trat ihr Vater die Nachfolge von Robert Bunsen an der Heidelberger
Universität an, schied aber 1897 während einer schweren depressiven
Episode freiwillig aus dem Leben.  Das Haus in der Bergstraße 32 ließ
Irmingards Mutter nach seinem Tod 1899 erbauen.

Später zog dort Irmingards Mann Guido_ ein, ab 1915 lebte dort auch
das gemeinsame Kind Conrad_.  Irmingards Mutter verließ
Heidelberg 1923 und starb 1936.

Die Eheleute Leser zogen 1933 von der Bergstraße in das Haus von Ida
Rothschild in die Roonstraße 10/12, das diese 1933 in zwei Wohneinheiten
umbauen ließ. Im Juni 1937 verkauften sie das schöne Haus in der
Bergstraße 32.

1936 zogen Irmingard Leser und ihr Mann nach Berlin, möglicherweise,
weil sie glaubten, in einer Großstadt sicherer zu sein.   In der Tat
entgingen sie so der `Wagner-Bürckel-Aktion`_ von 1940, während der die
lokalen Behörden fast alle JüdInnen aus Baden deportierten.  Doch
erreichten die Deportationen 1942 auch Berlin.
In Kenntnis des Grauens, das sie erwartete, tötete sich das Ehepaar
Leser mit Schlaftabletten, als die Behörden sie aufforderten, sich
zur Deportation nach Theresienstadt zu melden.

.. _Wagner-Bürckel-Aktion: http://de.wikipedia.org/wiki/Wagner-B%C3%BCrckel-Aktion

„Dr. Guido Leser und seine Ehefrau Irmingard, geb. Meyer, sind am 26.
Oktober 1942 in Berlin durch die Gestapo in den Tod getrieben worden“,
schrieb Walter Leser am 28. Juli 1948 über den Suizid seiner
Schwägerin und seines Bruders an das Amtsgericht Mannheim „In Sachen
Wiedergutmachung“.

Die Grabstätte des Ehepaars Leser befindet sich auf dem Jüdischen
Friedhof Berlin-Weißensee.

`Mehr zur Familie Leser
<http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2015/2015_Familie_Leser.pdf>`_

.. _Conrad: leser-c.html
.. _Guido: leser-g.html
.. _Victor Meyer: https://de.wikipedia.org/wiki/Victor_Meyer
