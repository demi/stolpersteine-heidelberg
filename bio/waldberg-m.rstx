Max Freiherr von Waldberg
=========================

.. container:: dates

  -REPLACE-WITH-DATES-

.. figure:: waldberg-m.jpeg

   Max von Waldberg (Foto: Robert Herbst, Quelle: Universitätsarchiv
   Heidelberg, BA Pos I 03161)

Max von Waldberg entstammte der gebildeten jüdischen Oberschicht des
damaligen Fürstentums Moldau, ab 1859 Bestandteil des nachmaligen
Königreichs Rumänien. Seine Vorfahren väterlicher- und mütterlicherseits
hatten sich dort Vermögen und hohes Ansehen erarbeitet. Sie gehörten der
deutsch-jüdischen Generation an, die in der Gedankenwelt Schillers und
der deutschen Sprache lebten und die Einbindung der jüdischen Religion
in die Lebensweise des Landes propagierten. 

Nach einem Studium der deutschen Philologie, Geschichte und Philosophie
in Czernowitz und Berlin arbeitete er als außerordentlicher Professor in
Czernowitz. Enttäuscht von den dortigen beschränkten Arbeitsbedingungen,
richtete Waldberg 1889 ein Gesuch an die philosophische Fakultät
Heidelberg, ihn als Privatdozenten für Neuere Deutsche Sprache und
Literatur zu übernehmen. Da es hierfür noch keinen Spezialisten gab,
wurde er in Baden als Privatdozent, dann 1893 als a.o. Professor mit
einer jährlichen Vergütung von 1000 Mark angenommen. Seine Position in
Heidelberg war indessen enttäuschend instabil, weitere Versuche in
Marburg und Bonn scheiterten. Die Diskriminierung der Juden an
preußischen Universitäten unterlag einem stillen Grundkonsens.

Seine Situation verbesserte sich auch nicht durchgreifend, als ihm 1908
der Titel „ordentlicher Honorarprofessor“ verliehen wurde. „Die Fakultät
hält daran fest, dass zu Honorarprofessoren nur Persönlichkeiten ernannt
werden sollen, die der Fakultät ehrenhalber ohne Gehalt eingegliedert
werden“ (aus den Akten der phil. Fak 1919-1920).  Max von Waldberg
konnte seiner Berufung nachgehen, weil er, von Hause aus vermögend,
nicht auf Gehalt angewiesen war und weil `seine Ehefrau Violetta`_ eine
beachtliche Aussteuer mit in die Ehe gebracht hatte.

Er erwies sich als ausgezeichneter Erzieher zur praktischen methodischen
Forschung und brachte den Studierenden das Handwerk bei, das andere
schlicht voraussetzten. Neidlos unterstützte und förderte er die
Habilitation des späteren Stargermanisten Friedrich Gundolf, und da
Gundolf das Privileg zugestanden wurde, weder lehren noch prüfen zu
müssen, fiel auch die Betreuung der Studierenden Gundolfs Waldberg zu.
Wer promovieren wollte, musste zu Waldberg gehen.

Unter seinen
Doktoranden sind etliche hochrangige Wissenschaftler, aber auch andere,
z.B. `Joseph Goebbels`_. Das Dissertationsgutachten Waldbergs
attestierte ihm „journalistische Begabung“. Die mündliche Prüfung
bestand er mit „rite“, gerade noch. Der geduldige Doktorvater wurde von
Dr.  Goebbels ab 1933 als Jude nicht mehr erwähnt, geschweige denn in
den folgenden Jahren vor Schlimmerem bewahrt.

.. _Joseph Goebbels: http://de.wikipedia.org/wiki/Joseph%20Goebbels

Nach großen finanziellen Verlusten im Gefolge der Hyperinflation Anfang
der 20er Jahre  sah sich Waldberg, der immer noch fast keine Vergütung
von der Universität erhielt, im Jahr 1926 gezwungen, sein Haus gegen
eine Leibrente von 7000 Goldmark dem Land Baden zu überlassen. Dafür
wurde dem kinderlosen Ehepaar der lebenslange Nießbrauch bis zu
Waldbergs Tod zugestanden.

Als am 7. April 1933 mit dem Gesetz zur „`Wiederherstellung des
Berufsbeamtentums`_“ auch der „nichtarische“ Honorarprofessor von Waldberg
in den Ruhestand versetzt wurde, verzichtete er am 12. April auf die
bereits angekündigte Vorlesung, „um unliebsames Aufsehen zu vermeiden
und der Universität und der Fakultät Schwierigkeiten zu ersparen.“
Immerhin versuchte der damalige Dekan der philosophischen Fakultät im
Juli 1933 zweimal mit Schreiben an das Kultusministerium, den Entzug
der Lehrbefugnis rückgängig zu machen. Dennoch und trotz weiterer
Interventionen von Fakultätskollegen wurde Waldbergs Name aus der Liste
der Dozenten gestrichen. 

.. _Wiederherstellung des Berufsbeamtentums: http://de.wikipedia.org/wiki/Gesetz%20zur%20Wiederherstellung%20des%20Berufsbeamtentums

Waldbergs Kollege Friedrich Panzer versuchte im Einverständnis mit dem
Rektor Prof. Willy Andreas in einem Brief an Cosima Wagners Tochter
Daniela Thode und mit Hinweis auf die „untadelige“ nationale Gesinnung
Waldbergs einen Weg über die Nachkommen des von Waldberg verehrten
Richard Wagner, um ihm die schwere Kränkung zu ersparen.  Eben war ein
von Waldberg herausgegebener Briefwechsel aus dem Hause Wagner
erschienen – die Hilfe jedoch blieb aus. Daniela Thode äußerte sich
anderweitig voller Mitleid, Winifred Wagner verweigerte sich.

Im September 1933 kam der Bescheid, dass die Eheleute von Waldberg, seit
dem 2. Juli 1919 Bürger Badens, aus rassischen Gründen ausgebürgert
werden sollten.  Dank dem erneuten persönlichen Einsatz und einem
eindrücklichen Schreiben des Rektors Andreas wurde von der Ausbürgerung
abgesehen.

Ab 14. November 1935 galt das Ehepaar von Waldberg gemäß den
Verordnungen zum Reichsbürgergesetz als „volljüdisch“, mit allen damit
verbundenen Konsequenzen. Max von Waldberg ertrug den Zusammenbruch
seiner Existenz und seiner ganzen Welt mit Gefasstheit und Noblesse.
Aber seine Forschungsarbeit war ihm sinnlos geworden, und so verbrannte
er seine gesamten Vorarbeiten zur Geschichte des Romans und andere
Manuskripte. Nach einer Krebsoperation starb er am 6. November 1938. Die
Urne mit seiner Asche wurde in aller Stille auf dem Heidelberger
Bergfriedhof beigesetzt.

`Mehr zum Ehepaar Waldberg`_

.. _Mehr zum Ehepaar Waldberg: http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2020/53_von_Waldberg_Text.pdf
.. _seine Ehefrau Violetta: waldberg-v.html
