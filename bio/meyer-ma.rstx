Margot Meyer
============

.. container:: dates

  -REPLACE-WITH-DATES-

.. figure:: meyer-ma.jpeg

   (Generallandsarchiv Karlsruhe, Personalakte)

Margot Meyer wurde am 30. April 1909 als Tochter des Kaufmanns Max Meyer
und seiner Ehefrau Martha Haas in Heidelberg geboren.  Ihre Eltern
betrieben in der  Akademiestraße 2  das Geschäft  „Max  Meyer -
Schneidereibedarfsartikel  en gros und en detail“ und wohnten über dem
Geschäft.  Dort wuchs Margot – seit 1919 mit ihrer Schwester Ilse – auf. 


Margot besuchte ab Herbst 1918 die Mädchenrealschule und wechselte
Ostern 1922 in die Untertertia des Mädchen-Realgymnasiums, wo sie 1928
die Reifeprüfung ablegte.

1929 starb Margots Vater 52-jährig an einer Sepsis.  Daraufhin führte
ihre Mutter das Geschäft weiter, wie sie es auch schon während des
Kriegseinsatzes von Max Meyer im ersten Weltkrieg getan hatte.

Margot Meyer begann im Wintersemester 1928/29 in Heidelberg ein Studium
der Zahnmedizin, wechselte aber bald zur Philologie und hörte u.a. bei
`Max von Waldberg`_ „Geschichte und Technik des Dramas“. Im
Sommersemester 1929 war sie an der Universität Berlin eingeschrieben,
setzte danach ihr Studium in Heidelberg fort und bestand ihr Examen mit
den Hauptfächern Deutsch und Englisch, Nebenfach Französisch mit der
Gesamtnote „ziemlich gut“. 

Sie muss sich für den schulischen Vorbereitungsdienst beworben haben,
denn die Schulbehörde vermerkte, „wegen eher mäßiger Leistungen“ könne
bei der großen Anzahl von Bewerbern ihre Übernahme in den badischen
höheren Schuldienst nicht in Betracht kommen, bis 31. Dezember 1932
könne sie sich aber auf freiwilliger Basis zum Vorbereitungsdienst der
Lehramtsreferendare melden; der Vermerk – zur Wiedervorlage – ist am 2.
Januar 1933 als „erl.“ abgezeichnet. Am 16. Januar 1933 wurde ihr der
Beamteneid abgenommen, am 17. Januar trat sie ihren Vorbereitungsdienst
als Lehramtsreferendarin an. 

Am 18. April 1933 erging das Schreiben des Kultusministeriums an die
Direktion der Mädchenrealschule mit der Weisung, `Berta Eisenmann`_ und
Margot Meyer zu entlassen. Der Passus „unter Wahrung der in §1 Ziffer 4
vorgesehenen  Frist  von  14  Tagen“  ist  durchgestrichen,  am  Rand
handschriftlich vermerkt: „beziehen keine Unterhaltszuschüße!“

Die  Entlassung  wurde  Margot  Meyer  am 21. April mitgeteilt. Ihre
Schwester berichtet, zwei ihrer  ehemaligen  Lehrerinnen  hätten  bei
ihr  zu Hause mit  Blumen  auf  sie gewartet,  um  sie zu trösten. 

Am 30. April schrieb sie an das Ministerium mit der Bitte, den Dienst
bis zum Assessorexamen zu Ende führen zu dürfen; sie hoffe, den Beruf
dann in privater Erziehertätigkeit ausüben zu können:

.. pull-quote::

  Über meine Gesinnung gibt die Mädchenrealschule  Heidelberg,  der  ich
  neun  Jahre  als Schülerin und drei Monate als Referendarin angehörte,
  Auskunft. Auch ist mein verstorbener Vater als ungedienter Soldat zum
  Schutze seiner deutschen Heimat an der Front gewesen.

Am 30. Juni antwortete das Kultusministerium:

.. pull-quote::
  
  Nach den in meinem Geschäftsbereich allgemein getroffenen Anordnungen
  werden nichtarische Bewerber zum Vorbereitungsdienst für öffentliche
  Laufbahnen nicht zugelassen. Es ist nicht möglich, in Ihrem Fall eine
  Ausnahme zu machen.

Margot Meyer zog daraufhin nach London, wo sie weiter mit dem Ziel
studierte, Lehrerin zu werden.  Sie wanderte 1934 aus England nach
Palästina aus, arbeitete als Englischlehrerin in Tiberias, später in Tel
Aviv. 

1943 heiratete sie Dr. jur. Rudolf Baer aus Wittlich, der in Israel als
Kaufmann arbeitete. Er starb am 6. Dezember 1967. In zweiter Ehe war sie
mit dem Journalisten und Übersetzer Erich Lehmann verheiratet. Sie starb
am 17. März 1983 und wurde in Tel Aviv, Cholon begraben.


`Mehr zu Margot Meyer und ihrer Familie <http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2017/45_Margot_Meyer_neueVorlage.pdf>`_

.. _Max von Waldberg: waldberg-m.html
.. _Berta Eisenmann: baer-be.html
