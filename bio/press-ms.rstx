Max Samuel Press-Simon
======================

.. container:: dates

  -REPLACE-WITH-DATES-

.. figure:: press-m-s.jpeg

  Max Samuel Press und seine Frau Sophie 1927

Max Samuel Preß (oder Press) wurde am 1. März 1904 in Karlsruhe geboren als Kind
von Hirsch Press und Sara Press, geb. Simon.  Im elterlichen Haushalt
befindet sich zeitweise auch Jakob Preß unter gleicher Adresse mit einem
Geschäft für Haushalts- und Küchengeräte, Öfen, Herde etc. 1925 ist S.
Press – also möglicherweise (Max) Salomon Press, zumal sein Vater
bereits 1919 gestorben war – als Eigentümer des Geschäfts genannt.

Am 4. Juni  1927 heiratete Max Samuel Press in Heidelberg `Sophie
Weiner`_, am 24. November  1928 brachte diese die gemeinsame Tochter
Ruth_ zur Welt.

Im Heidelberger Adressbuch findet sich von 1931 bis 1933 unter der
Adresse Hauptstraße 123 der Eintrag Preß, Max, Kaufmann. Die Familie
bewohnte das Haus mit insgesamt vier Parteien – neben anderen mit der
verschwägerten Familie von Max Rennert, der ein Ausstattungsgeschäft
betrieb.

Die Familie hat Deutschland bereits am 1. Juli 1933 verlassen und ist
nach Israel emigriert.  Sie hatte die  aggressionsgeladene Stimmung
etwa am  1. April 1933 miterlebt, als die NS-Regierung die
Boykottaktionen gegen jüdisch geführte Geschäfte anlaufen ließ.


Die Zeitzeugin Frieda Hirsch schildert die Atmosphäre, vor der die
Familie Press-Simon floh:

.. pull-quote::

  Wir Juden konnten die Schrecknisse zuerst einfach nicht für möglich
  halten. Jüdische Eltern ließen ihre Kinder noch im Zug mitmarschieren.
  Aber am 1. April, als die „spontane Volkswut“ verlangte, daß
  „schlagartig“ alle jüdischen Geschäfte und Betriebe geschlossen würden
  und alle Ärzte und Juristen Naziwachen vor's Haus bekamen, da wussten
  wir Bescheid.

Max Samuel Press nahm in Israel den Namen Simon an.  Er starb dort im
Jahr 1979.

`Mehr zur Familie Press-Simon <http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2017/Stolpersteinbroschuere_2017.pdf>`_
ab Seite 27

.. _Sophie Weiner: simon-s.html
.. _Ruth: simon-r.html
