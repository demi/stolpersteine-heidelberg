Doris Ellen Baer
================

.. container:: dates

  -REPLACE-WITH-DATES-

Doris Ellen Baer wurde am 21. Oktober 1923 als Tochter des Mannheimer
Landgerichtsrats `Alfred Baer`_ und dessen Frau `Klara`_ geboren.  1925
brachte ihre Mutter ihren Bruder `Hans Dieter`_ zur Welt.

.. _Alfred Baer: baer-al.html
.. _Klara: baer-kl.html
.. _Hans Dieter: baer-hd.html

Nachdem die NSDAP-Regierung ihren Vater mit den `Nürnberger Gesetzen`_
aus dem Staatsdienst entfernt hatte, zog die Familie 1935 in das Haus in
der Dantestraße (damals: Kronprinzenstraße) 24.

.. _Nürnberger Gesetzen: http://de.wikipedia.org/wiki/N%C3%BCrnberger%20Gesetze

Doris Ellen Baer besuchte bis November 1938 das (seit 1937 so heißende)
Kurfürst-Friedrich-Gymnasium.  Sie wurde im Gefolge der Pogrome vom 9.
November auf Weisung_ des Reichserziehungsministers Rust vom 15.
November 1938 von der Schule ausgeschlossen, da

.. pull-quote::

  keinem deutschen Lehrer und keiner deutschen Lehrerin mehr zugemutet
  werden [kann], an jüdische Kinder Unterricht zu erteilen […] Auch
  versteht es sich von selbst, daß es für deutsche Schüler und
  Schülerinnen unerträglich ist, mit Juden in einem Klassenraum zu sitzen.

.. _Weisung: http://digitalpast.de/2013/11/15/weisung-des-reichserziehungsministers-rust-vom-15-november-1938/

Ihre Eltern konnten sie im März 1939 mit einem der Kindertransporte_
nach England retten.  1940
deportierten die deutschen Behörden ihre Eltern ins Lager Gurs_.  Vom Tod
ihres Vaters im Lager Récébédou erfuhr sie noch aus einem Brief ihrer
Mutter, die Ermordung der Mutter in Auschwitz im August 1942 wurde erst
nach dem Krieg Gewissheit.

.. _Kindertransporte: http://de.wikipedia.org/wiki/Kindertransport
.. _Gurs: http://de.wikipedia.org/wiki/Camp%20de%20Gurs

Doris Ellen Baer hat in England weiter die Schule besucht, wurde
Krankenschwester und wanderte 1950 in die USA aus.

`Mehr zu Familie Baer <http://stolpersteine-heidelberg.de/familie-baer.html>`_
