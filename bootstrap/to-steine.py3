# http://stolpersteine-heidelberg.de/biographie-und-orte.html (das
# nach upstream.html gedumpt werden muss) in
# lat\tlong\tName\tAdresse\tMedia-Link
# wandeln; das habe ich am Anfang ein Mal gemacht.  steine.tsv hat
# inzwischen haufenweise lokale Änderungen, so dass das wahrscheinlich
# keinen Wert mehr haben wird.

from urllib.parse import urljoin
import re

from bs4 import BeautifulSoup

SOURCE_URL = "http://stolpersteine-heidelberg.de/biographie-und-orte.html"


with open("upstream.html") as f:
	soup = BeautifulSoup(f, "lxml")
	# Das Quelldokument ist ziemlich unstrukturiert; meine Heuristik: 
	# ich suche nach Georeferenzierung (was jedenfalls fast alle bekommt)
	# und nehme dann die Personeninfo aus dem A-Link davor
	for link in soup.find_all("a"):
		parts = []
		href = link.get("href") or ""
		if re.search("@([\d.]+),([\d.]+)",  href):
			# Wird wohl ein google-Link sein, aber die echten Koordinaten sind,
			# glaube ich, in !3d und !4d-Gruppen (aber: keine Ahnung, ist durch
			# hinschauen bestimmt)
			parts = [re.search("!3d([\d.]+)", href).group(1),
				re.search("!4d([\d.]+)", href).group(1)]
			place_link = link
		else:
			# kein Geo-link, weitermachen
			continue

		neetag = ""

		for el in place_link.previous_siblings:
			if el.name=="a":
				person_link = el
				break
			elif "geb." in el:
				neetag = el.strip().rstrip(",")

		parts.append(person_link.text+neetag)
		parts.append(place_link.text)
		parts.append(urljoin(SOURCE_URL, person_link.get("href")))

		print("\t".join(parts))
