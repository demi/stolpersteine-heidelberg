build: make-page.py3 steine.tsv leaflet
	python3 make-page.py3 $@
	cp leaflet/leaflet.js leaflet/leaflet.css $@
	cp -r leaflet/images $@
	cp markercluster/leaflet.markercluster.js markercluster/MarkerCluster.css\
	  markercluster/MarkerCluster.Default.css $@
	(cd bio; make)
	rsync -av bio build/
	rsync -av stein-foto build/

install: build
	rsync -av build/ sosa:/var/www/htdocs/steine/

clean:
	rm -rf build
	(cd bio; make clean)
