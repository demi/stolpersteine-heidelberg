Eine interaktive Karte von Stolpersteinen in Heidelberg.

Technische Basis: Leaflet.js, https://leafletjs.com/; weil das
schmerzhaft zu bauen ist (npm), ist eine stabile Version davon
(von https://leafletjs.com/download.html) lokal hier drin, und make
build kopiert die nötigen Ressourcen von dort nach build.

Quellen: http://stolpersteine-heidelberg.de


Kurzbios
========

Weil die gemeinsamen Darstellungen der Stolperstein-Ini für die
einzelnen Personen nicht gut verwenden können, haben wir eigene
Kurzbiografien im Unterverzeichnis bio.  Die sollten eine gemeinsame
Struktur haben:

Vorname Nachname
================

.. container:: dates

  -REPLACE-WITH-DATES-

.. figure:: bild.jpeg

   Caption fürs Bild

Haupt-Text

`Mehr zu XY <http://stolpersteine-heidelberg.de/mediapool/63/638182/data/2022/...>`_

.. _Gurs: http://de.wikipedia.org/wiki/Camp%20de%20Gurs
.. _Wagner-Bürckel-Aktion: http://de.wikipedia.org/wiki/Wagner-B%C3%BCrckel-Aktion
.. _Noé: http://de.wikipedia.org/wiki/No%C3%A9%20%28Haute-Garonne%29
.. _Récébédou: http://de.wikipedia.org/wiki/Portet-sur-Garonne
.. _Wiederherstellung des Berufsbeamtentums: http://de.wikipedia.org/wiki/Gesetz%20zur%20Wiederherstellung%20des%20Berufsbeamtentums
.. _Hermann Maas: http://de.wikipedia.org/wiki/Hermann%20Maas%20%28Theologe%29
.. _Polenaktion: http://de.wikipedia.org/wiki/Polenaktion
.. _Nürnberger Gesetze: https://de.wikipedia.org/wiki/N%C3%BCrnberger_Gesetze
.. _Giovannini, Rink und Moraw: https://www.wunderhorn.de/?buecher=erinnern-bewahren-gedenken
.. _Judenhaus: http://de.wikipedia.org/wiki/Judenhaus
.. _Grafeneck: http://de.wikipedia.org/wiki/T%C3%B6tungsanstalt%20Schloss%20Grafeneck
.. _Aktion T4: https://de.wikipedia.org/wiki/Aktion_T4
.. _Konzentrationslager Dachau: http://de.wikipedia.org/wiki/KZ%20Dachau
.. _Pogromnacht: http://de.wikipedia.org/wiki/Novemberpogrom%201938
.. _Vichy: http://de.wikipedia.org/wiki/Vichy-Regime
.. _Theresienstadt: http://de.wikipedia.org/wiki/KZ%20Theresienstadt
.. _Hadamar: https://de.wikipedia.org/wiki/T%C3%B6tungsanstalt_Hadamar
.. _Westerbork: http://de.wikipedia.org/wiki/Durchgangslager%20Westerbork
.. _Nürnberger Prozesse: https://de.wikipedia.org/wiki/N%C3%BCrnberger_Prozesse
.. _Gesetz über die Änderung von Familiennamen und Vornamen: https://de.wikipedia.org/wiki/Namens%C3%A4nderungsverordnung


Wenn es wirklich mal ein seitenfüllendes Bild braucht:

.. figure:: fulltext.png
  :figclass: full



Stein-Fotos
===========

Wir sammeln Fotos der Steine; das sollen jpegs mit 600x600 Pixeln sein,
wenns geht deprojeziert.  Die Fotos liegen in stein-foto, die Dateinamen
sollten den Biographie-Namen entsprechen -- aber das ist nur
Bequemlichkeit, die Zuordnung erfolgt in Spalte 7.


Koordinaten
===========

Stolpersteine sind draußen im Gelände nicht sort, wo die Koordinaten der
Ini sie sehen, oft ziemlich viele Meter entfernt.  Um ordentliche
Koordinaten zu bekommen, die dann auch in der OSM richtig aussehen,
empfiehlt sich folgendes Vorgehen:

(a) Wegpunkt setzen

(b) Wegpunkt in josm anfahren

(c) Stolperstein in josm anlegen, mit tag historic=memorial,
    memorial=stolperstein, name=eine Person.  Wie wir mit mehreren
    beieinanderliegenden Stolpersteinen umgehen, müssen wir noch
    überlegen.  Dabei sehen, dass das Ding auf der Karte dort zu
    liegen kommt, wo Leute es auch finden.

(d) Koordinaten aus josm in steine.tsv übernehmen (Edit/Copy
    Coordinates); beim Suchen-und-Ersetzen immer schauen, dass Tabs nach
    bzw.  vor und nach den Koordinatenwerten sind.


Mapproxy
========

Aus Datenschutzgründen, und im die OSM-Ressourcen zu schonen, nehmen wir
die Tiles aus einem großzügig konfigurierten Cache, mapproxy.  Die
Konfig ist auf sosa in /srv/mapproxy/osmproxy/mapproxy.yaml (TODO:
config hier ins VCS tun, wenn das alles funktioniert).

Der Mapproxy läuft im gunicorn als WSGI-container hinter dem nginx als
reverse proxy (auf victor ist das einfach ein WSGIScriptAlias).

Das Ding startet dann auf sosa manuell als in /srv/mapproxy/osmproxy mit::

  gunicorn -b 127.0.0.1:9080 server:application

Wenn eh ein Apache läuft, ist es einfacher, so etwas zu machen::

  WSGIScriptAlias /tiles /srv/mapproxy/osmproxy/server.py
  <Directory /srv/mapproxy/osmproxy>
    Require all granted
  </Directory>

Die Tiles kommen dann auf tms/1.0.0/osm/{z}/{x}/{y}.png raus.

Dazu gehören aus unserem tilecache-Subdir::

* mapproxy.yaml und server.py, die nach /srv/mapproxy/osmproxy müssen
  (dabei entstand server.py durch ``mapproxy-util create -t wsgi-app -f
  mapproxy.yaml server.py``).
* mapproxy als initscript (wo der Kram nicht aus einem Apache raus läuft)


Daraus wird das server.py, das es oben braucht, durch::

  mapproxy-util create -t wsgi-app -f mapproxy.yaml server.py

Wichtig dabei: /srv/mapproxy/osmproxy/cache_data muss www_data gehören.


Um die Tiles direkt von der OSM zu ziehen,

  URL: https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png

in die STEIN_MAP in make-page.py3 schreiben und zoomOffset und tms
auskommentieren.


History
=======

Bootstrap der steine.tsv von
http://stolpersteine-heidelberg.de/biographie-und-orte.html


